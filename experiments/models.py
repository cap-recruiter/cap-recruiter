import asyncio
import logging
import time
from datetime import date, timedelta

import aiohttp
from aiohttp import ClientConnectorError
from aiohttp.client import ClientSession
from auditlog.registry import auditlog
from django.db import models
from django.db.models import Sum
from django.urls import reverse
from django.utils import timezone
from django_fsm import FSMField, transition
from django_request_cache import cache_for_request

from .utils import get_scheme_and_netloc

logger = logging.getLogger(__name__)


class Experiment(models.Model):
    title = models.CharField(max_length=255, db_index=True, null=False, blank=False)
    description = models.TextField(null=False, blank=True)
    url = models.CharField(max_length=2048, db_index=True, null=False, blank=True)
    username = models.CharField(max_length=64, null=True, blank=True)
    password = models.CharField(max_length=32, null=True, blank=True)
    use_for_test_tasks = models.BooleanField(default=False, blank=True)
    batches = models.IntegerField(null=False, default=1)
    estimated_duration = models.IntegerField(null=False, blank=False, default=0)
    max_duration = models.IntegerField(null=True, blank=True)
    notes = models.TextField(null=False, blank=True, default="")

    # foreign keys
    create_user = models.ForeignKey(
        "accounts.CustomUser",
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="cap_recruiter_experiment_create_user_id",
    )
    update_user = models.ForeignKey(
        "accounts.CustomUser",
        null=True,
        on_delete=models.SET_NULL,
        related_name="cap_recruiter_experiment_update_user_id",
    )

    # timestamps
    start_date = models.DateField()
    expire_date = models.DateField()
    created_at = models.DateTimeField(db_index=True, auto_now_add=True)
    updated_at = models.DateTimeField(db_index=True, auto_now=True)
    deleted_at = models.DateTimeField(db_index=True, null=True)

    def sorted_group_managers(self):
        unsorted_group_managers = [
            group_manager for group_manager in self.group_managers.all()
        ]
        return sorted(
            unsorted_group_managers, key=lambda group_manager: group_manager.full_name()
        )

    def days_until_expiry(self):
        return (self.expire_date - date.today()).days

    def days_until_start(self):
        return (self.start_date - date.today()).days

    def get_absolute_url(self):
        return reverse("experiments-experiments")

    def tasks_count(self):
        return self.tasks.count()

    def worker_name(self):
        return self.worker.full_name()

    def base_payment_sum(self):
        return self.tasks.aggregate(Sum("base_payment"))["base_payment__sum"]

    def bonus_sum(self):
        return self.tasks.aggregate(Sum("bonus"))["bonus__sum"]

    def total_base_payments(self):
        tasks = self.tasks

        if not tasks:
            return 0
        return tasks.aggregate(Sum("base_payment")).get("base_payment__sum")

    def total_bonuses(self):
        tasks = self.tasks

        if not tasks:
            return 0
        return tasks.aggregate(Sum("bonus")).get("bonus__sum")

    def total_payments(self):
        tasks = self.tasks

        if not tasks:
            return 0

        base_payment_sum = tasks.aggregate(Sum("base_payment")).get("base_payment__sum")
        if base_payment_sum is None:
            return 0

        bonus_sum = tasks.aggregate(Sum("bonus")).get("bonus__sum")

        return bonus_sum + base_payment_sum

    def total_sum(self):
        if None in [self.base_payment_sum(), self.bonus_sum()]:
            return 0
        return self.base_payment_sum() + self.bonus_sum()

    def has_expired(self):
        return self.expire_date < date.today()

    def is_deleted(self):
        return self.deleted_at is not None

    def is_active(self):
        if (
            self.is_deleted()
            or self.has_expired()
            or (not self.has_group_managers_assigned() and not self.use_for_test_tasks)
        ):
            return False
        return True

    def is_active_and_deployed(self):
        return self.is_active() and self.id in Experiment.deployed_experiments()

    def has_group_managers_assigned(self):
        if len(self.group_managers.all()) > 0:
            return True
        return False

    def dashboard_url(self):
        return f"{get_scheme_and_netloc(self.url)}/dashboard"

    def test_tasks_experiment(self):
        return self.objects.filter(self.initial_test_experiment_id).first()

    @classmethod
    @cache_for_request
    def deployed_experiments(cls):
        async def get_deployed_experiment_id(url: dict, session: ClientSession):
            try:
                async with session.get(url["url"]) as response:
                    if response.status == 200:
                        return url["id"]
            except ClientConnectorError:
                return None

        async def get_deployed_experiment_ids(urls: list):
            my_conn = aiohttp.TCPConnector(limit=10)
            async with aiohttp.ClientSession(connector=my_conn) as session:
                tasks = []
                for url in urls:
                    task = asyncio.ensure_future(
                        get_deployed_experiment_id(url=url, session=session)
                    )
                    tasks.append(task)
                responses = await asyncio.gather(*tasks, return_exceptions=True)
            return responses

        url_list = [
            {"id": experiment.id, "url": get_scheme_and_netloc(experiment.url)}
            for experiment in cls.objects.exclude(deleted_at__isnull=False).order_by(
                "-created_at"
            )
        ]
        start = time.time()
        experiment_ids = [
            _id
            for _id in asyncio.run(get_deployed_experiment_ids(url_list))
            if _id is not None
        ]
        end = time.time()
        logger.info(
            f"Deployed experiment ids {experiment_ids} for {len(url_list)} URLs returned in {end - start} seconds."
        )

        return experiment_ids

    @classmethod
    def exists_deployed_experiment(cls):
        if not cls.deployed_experiments():
            return False
        return True


class Payment(models.Model):
    state = models.CharField(max_length=20, db_index=True, null=False)

    STATES = (
        ("paid", "Paid"),
        ("received", "Received"),
        ("unpaid", "Unpaid"),
    )
    state = FSMField(default="unpaid", protected=True, choices=STATES)

    task = models.OneToOneField(
        "Task",
        null=False,
        on_delete=models.CASCADE,
        primary_key=True,
        related_name="payment",
    )

    worker = models.ForeignKey(
        "accounts.CustomUser",
        null=False,
        on_delete=models.CASCADE,
        related_name="payments",
    )

    amount = models.FloatField(default=0)

    # timestamps
    paid_at = models.DateTimeField(db_index=True, null=True)
    received_at = models.DateTimeField(db_index=True, null=True)

    # state transitions
    @transition(field=state, source="unpaid", target="paid")
    def pay(self):
        pass

    @transition(field=state, source="paid", target="received")
    def receive(self):
        pass


class Task(models.Model):
    state = models.CharField(max_length=20, db_index=True, null=False)
    assignment_id = models.CharField(max_length=200, db_index=True, null=False)

    # foreign keys
    experiment = models.ForeignKey(
        Experiment,
        null=False,
        on_delete=models.CASCADE,
        related_name="tasks",
    )
    group_manager = models.ForeignKey(
        "accounts.GroupManager",
        null=False,
        on_delete=models.CASCADE,
        related_name="tasks",
    )
    worker = models.ForeignKey(
        "accounts.CustomUser",
        null=False,
        on_delete=models.CASCADE,
        related_name="tasks",
    )

    STATES = (
        ("started", "Started"),
        ("completed", "Completed"),
        ("not_completed", "Not completed"),
        ("payment_done", "Payment done"),
        ("payment_received", "Payment received"),
        ("reset", "Reset"),
    )
    state = FSMField(default="started", protected=True, choices=STATES)

    # state transitions
    @transition(field=state, source=["started", "not_completed"], target="completed")
    def complete(self):
        pass

    @transition(field=state, source="started", target="not_completed")
    def fail(self):
        pass

    @transition(
        field=state, source=["completed", "not_completed"], target="payment_done"
    )
    def pay(self):
        pass

    @transition(field=state, source="payment_done", target="payment_received")
    def finalize(self):
        pass

    @transition(field=state, source="not_completed", target="reset")
    def reset(self):
        pass

    base_payment = models.FloatField(default=0)
    bonus = models.FloatField(default=0)
    comment = models.TextField(blank=True, default="")
    batch = models.IntegerField(null=False, default=1)
    failed_reason = models.TextField(blank=True, default="")

    # timestamps
    reset_at = models.DateTimeField(db_index=True, null=True)
    started_at = models.DateTimeField(db_index=True, auto_now_add=True)
    completed_at = models.DateTimeField(db_index=True, null=True)
    failed_at = models.DateTimeField(db_index=True, null=True)
    deleted_at = models.DateTimeField(db_index=True, null=True)
    payment_done_at = models.DateTimeField(db_index=True, null=True)
    payment_received_at = models.DateTimeField(db_index=True, null=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["worker", "experiment", "batch", "reset_at"],
                name="unique_worker_experiment_batch_reset_at",
            ),
        ]

    def is_test_task(self):
        return self.experiment.use_for_test_tasks

    def experiment_title(self):
        return self.experiment.title

    def group_manager_code(self):
        return self.group_manager.code

    def group_manager_name(self):
        return self.group_manager.name()

    def group_manager_full_name(self):
        return self.group_manager.full_name()

    def participant_worker_id(self):
        return self.worker.worker_id

    def total_payment(self):
        return self.base_payment + self.bonus

    def participant_email(self):
        return self.worker.email

    def participant_name(self):
        return self.worker.full_name()

    def max_duration(self):
        return self.experiment.max_duration

    def max_duration_exceeded(self):
        if not self.max_duration():
            return False
        return self.started_at + timedelta(minutes=self.max_duration()) < timezone.now()

    def is_deleted(self):
        return self.deleted_at is not None


class Singleton(models.Model):
    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.pk = 1
        super(Singleton, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        pass

    @classmethod
    def load(cls):
        obj, _ = cls.objects.get_or_create(pk=1)
        return obj


class EditableSettings(Singleton):
    show_log = models.BooleanField(default=False)

    def get_absolute_url(self):
        return reverse("experiments-experiments")

    def __str__(self):
        return self.__dict__


auditlog.register(Experiment)
auditlog.register(Payment)
auditlog.register(Task)
