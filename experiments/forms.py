import logging
from datetime import date

from django import forms

from .models import EditableSettings, Experiment

logger = logging.getLogger(__name__)


class ExperimentCreationForm(forms.ModelForm):
    class Meta:
        model = Experiment
        fields = [
            "title",
            "description",
            "url",
            "username",
            "password",
            "batches",
            "estimated_duration",
            "max_duration",
            "notes",
            "start_date",
            "expire_date",
        ]

    title = forms.CharField(max_length=255, label="Title")
    description = forms.CharField(max_length=2048, label="Description")
    url = forms.CharField(max_length=2048, label="URL")
    username = forms.CharField(max_length=64, label="Username")
    password = forms.CharField(
        widget=forms.PasswordInput, max_length=32, label="Password"
    )
    estimated_duration = forms.IntegerField(
        label="Estimated duration (in minutes)",
        required=False,
        widget=forms.NumberInput(
            attrs={"placeholder": "Provide a time estimate", "min": 1}
        ),
    )
    max_duration = forms.IntegerField(
        label="Maximum duration (in minutes)",
        required=False,
        widget=forms.NumberInput(
            attrs={"placeholder": "Leave empty for infinite duration", "min": 1}
        ),
    )
    notes = forms.CharField(widget=forms.Textarea, label="Notes", required=False)
    start_date = forms.DateField(
        widget=forms.SelectDateWidget(years=range(date.today().year, 2030))
    )
    expire_date = forms.DateField(
        widget=forms.SelectDateWidget(years=range(date.today().year, 2030))
    )

    def __init__(self, *args, **kwargs):
        super(forms.ModelForm, self).__init__(*args, **kwargs)
        self.order_fields(
            [
                "title",
                "description",
                "start_date",
                "expire_date",
                "estimated_duration",
                "max_duration",
                "batches",
                "url",
                "dashboard_url",
                "username",
                "password",
                "notes",
            ]
        )

    def save(self, commit=True):
        experiment = super(forms.ModelForm, self).save(commit=False)
        experiment.save()
        if commit:
            try:
                experiment.group_managers.set(
                    dict(self.__dict__["data"])["group_managers"]
                )
            except KeyError:
                experiment.group_managers.set([])
                logger.warning(
                    f"Experiment with id '{experiment.id}'' has no groups assigned."
                )

            use_for_test_tasks = dict(self.__dict__["data"]).get("use_for_test_tasks")
            if use_for_test_tasks is not None and use_for_test_tasks[0] == "on":
                experiment.use_for_test_tasks = True
                experiment.initial_test_experiment_id = None
            else:
                experiment.use_for_test_tasks = False
                initial_test_experiment_id = dict(self.__dict__["data"]).get(
                    "initial_test_experiment"
                )
                if (
                    initial_test_experiment_id is not None
                    and initial_test_experiment_id[0] != ""
                ):
                    experiment.initial_test_experiment_id = int(
                        initial_test_experiment_id[0]
                    )
            experiment.save()

        return experiment


class ExperimentUpdateForm(forms.ModelForm):
    class Meta:
        model = Experiment
        fields = [
            "title",
            "description",
            "url",
            "username",
            "password",
            "batches",
            "estimated_duration",
            "max_duration",
            "notes",
            "start_date",
            "expire_date",
        ]

    title = forms.CharField(max_length=255, label="Title")
    description = forms.CharField(max_length=2048, label="Description")
    url = forms.CharField(max_length=2048, label="URL")
    dashboard_url = forms.CharField(
        widget=forms.TextInput(attrs={"style": "width: 95%; margin-bottom: 15px;"}),
        max_length=2048,
        label="Dashboard URL",
        disabled="disabled",
        required=False,
    )
    username = forms.CharField(
        widget=forms.TextInput(attrs={"style": "width: 95%; margin-bottom: 15px;"}),
        max_length=64,
        label="Username",
    )
    password = forms.CharField(
        widget=forms.PasswordInput(
            render_value=True,
            attrs={"style": "width: 95%; margin-bottom: 15px;"},
        ),
        max_length=32,
        label="Password",
    )
    estimated_duration = forms.IntegerField(
        label="Estimated duration (in minutes)",
        required=False,
        widget=forms.NumberInput(
            attrs={"placeholder": "Provide a time estimate", "min": 1}
        ),
    )
    max_duration = forms.IntegerField(
        label="Maximum duration (in minutes)",
        required=False,
        widget=forms.NumberInput(
            attrs={"placeholder": "Leave empty for infinite duration", "min": 1}
        ),
    )
    notes = forms.CharField(widget=forms.Textarea, label="Notes", required=False)
    start_date = forms.DateField(
        widget=forms.SelectDateWidget(years=range(date.today().year, 2030))
    )
    expire_date = forms.DateField(
        widget=forms.SelectDateWidget(years=range(date.today().year, 2030))
    )

    def __init__(self, *args, **kwargs):
        super(forms.ModelForm, self).__init__(*args, **kwargs)
        self.order_fields(
            [
                "title",
                "description",
                "start_date",
                "expire_date",
                "estimated_duration",
                "max_duration",
                "batches",
                "url",
                "dashboard_url",
                "username",
                "password",
                "notes",
            ]
        )

    def save(self, commit=True):
        experiment = super(forms.ModelForm, self).save(commit=False)
        experiment.save()
        if commit:
            try:
                experiment.group_managers.set(
                    dict(self.__dict__["data"])["group_managers"]
                )
            except KeyError:
                experiment.group_managers.set([])
                logger.warning(
                    f"Experiment with id '{experiment.id}'' has no groups assigned."
                )

            use_for_test_tasks = dict(self.__dict__["data"]).get("use_for_test_tasks")
            if use_for_test_tasks is not None and use_for_test_tasks[0] == "on":
                experiment.use_for_test_tasks = True
                experiment.initial_test_experiment_id = None
            else:
                experiment.use_for_test_tasks = False
                initial_test_experiment_id = dict(self.__dict__["data"]).get(
                    "initial_test_experiment"
                )
                if (
                    initial_test_experiment_id is not None
                    and initial_test_experiment_id[0] != ""
                ):
                    experiment.initial_test_experiment_id = int(
                        initial_test_experiment_id[0]
                    )
            experiment.save()

        return experiment


class EditableSettingsUpdateForm(forms.ModelForm):
    class Meta:
        model = EditableSettings
        fields = []
