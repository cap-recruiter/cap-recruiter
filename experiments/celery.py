import os

from celery import Celery
from decouple import config

# set the default Django settings module for the 'celery' program.
os.environ.setdefault(
    "DJANGO_SETTINGS_MODULE", f"cap_recruiter.settings.{config('DJANGO_ENV')}"
)

app = Celery("cap_recruiter", broker="redis://localhost")
app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks()


@app.task(bind=True)
def debug_task(self):
    print("Request: {0!r}".format(self.request))


app.conf.beat_schedule = {
    "check_experiment_duration": {
        "task": "check_experiment_duration_task",
        "schedule": 10.0,
    },
}
