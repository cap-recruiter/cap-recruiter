import ast
import asyncio
import json
import logging
import re
import sys
from datetime import datetime, timedelta, timezone

from asgiref.sync import sync_to_async
from channels.auth import login
from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncWebsocketConsumer
from channels.layers import get_channel_layer
from django.contrib.humanize.templatetags.humanize import naturaltime
from django.db.models import Q
from django.shortcuts import get_object_or_404
from django.utils.formats import date_format
from django.utils.timesince import timeuntil
from django.utils.timezone import localtime

from accounts.models import CustomUser

from .models import Payment, Task

channel_layer = get_channel_layer()
logger = logging.getLogger(__name__)


class AdminExperimentsConsumer(AsyncWebsocketConsumer):
    @database_sync_to_async
    def get_experiments(self):
        from experiments.models import Experiment

        return list(Experiment.objects.all().prefetch_related("tasks"))

    async def connect(self):
        self.groupname = "admin_experiments_group"
        await self.channel_layer.group_add(
            self.groupname,
            self.channel_name,
        )
        await self.accept()

    async def disconnect(self, code):
        await self.channel_layer.group_discard(
            self.groupname,
            self.channel_name,
        )
        await super().disconnect(code)

    async def send_admin_experiments(self, event):
        while True:
            experiments = await self.get_experiments()
            json_data = {}
            for experiment in experiments:
                json_data[experiment.id] = {
                    "tasks_count": (
                        experiment.tasks.count() if experiment.tasks.count() > 0 else ""
                    ),
                }

            await self.send(text_data=json.dumps(json_data))
            await asyncio.sleep(2)

    async def receive(self, text_data):
        from accounts.models import CustomUser

        text_data_json = json.loads(text_data)
        custom_user_id = text_data_json["custom_user_id"]
        custom_user = await sync_to_async(CustomUser.objects.get)(pk=custom_user_id)

        await login(
            self.scope, custom_user, backend="django.contrib.auth.backends.ModelBackend"
        )
        await database_sync_to_async(self.scope["session"].save)()
        await self.channel_layer.group_send(
            "admin_experiments_group",
            {
                "type": "send_admin_experiments",
            },
        )


class AdminParticipantsConsumer(AsyncWebsocketConsumer):
    @database_sync_to_async
    def get_participants(self):
        from accounts.models import CustomUser

        return list(CustomUser.objects.all().prefetch_related("tasks"))

    async def connect(self):
        self.groupname = "admin_participants_group"
        await self.channel_layer.group_add(
            self.groupname,
            self.channel_name,
        )
        await self.accept()

    async def disconnect(self, code):
        await self.channel_layer.group_discard(
            self.groupname,
            self.channel_name,
        )
        await super().disconnect(code)

    async def send_admin_participants(self, event):
        while True:
            participants = await self.get_participants()
            json_data = {}
            for participant in participants:
                json_data[participant.id] = {
                    "tasks_count": participant.tasks.count(),
                }

            await self.send(text_data=json.dumps(json_data))
            await asyncio.sleep(2)

    async def receive(self, text_data):
        from accounts.models import CustomUser

        text_data_json = json.loads(text_data)
        custom_user_id = text_data_json["custom_user_id"]
        custom_user = await sync_to_async(CustomUser.objects.get)(pk=custom_user_id)

        await login(
            self.scope, custom_user, backend="django.contrib.auth.backends.ModelBackend"
        )
        await database_sync_to_async(self.scope["session"].save)()
        await self.channel_layer.group_send(
            "admin_participants_group",
            {
                "type": "send_admin_participants",
            },
        )


class AdminTasksConsumer(AsyncWebsocketConsumer):
    @database_sync_to_async
    def get_experiment(self, experiment_id):
        from experiments.models import Experiment

        return get_object_or_404(Experiment, pk=experiment_id)

    @database_sync_to_async
    def get_experiment_tasks(self, experiment_id):
        from experiments.models import Task

        return list(Task.objects.filter(experiment_id=experiment_id).all())

    @database_sync_to_async
    def get_worker_tasks(self, worker_id):
        from experiments.models import Task

        return list(Task.objects.filter(worker_id=worker_id).all())

    @database_sync_to_async
    def get_outstanding_payments_in_dollars(self, worker_id):
        from accounts.models import CustomUser

        worker = CustomUser.objects.get(pk=worker_id)
        return worker.outstanding_payments_sum()

    async def connect(self):
        self.groupname = "admin_tasks_group"
        await self.channel_layer.group_add(
            self.groupname,
            self.channel_name,
        )
        await self.accept()

    async def disconnect(self, code):
        await self.channel_layer.group_discard(
            self.groupname,
            self.channel_name,
        )
        await super().disconnect(code)

    async def send_admin_tasks(self, event):
        from accounts.models import CustomUser

        experiment_id = event.get("experiment_id")
        worker_id = event.get("worker_id")
        custom_user = None

        if worker_id is not None:
            custom_user = await sync_to_async(CustomUser.objects.get)(pk=worker_id)
        has_outstanding_payments = None

        while True:
            if experiment_id is not None:
                tasks = await self.get_experiment_tasks(experiment_id)
            elif worker_id is not None:
                tasks = await self.get_worker_tasks(worker_id)
                outstanding_payments_in_dollars = (
                    await self.get_outstanding_payments_in_dollars(worker_id)
                )
                has_outstanding_payments = (
                    True if outstanding_payments_in_dollars > 0 else False
                )

            json_data = {}
            json_data["tasks"] = {}

            for task in tasks:
                experiment = await self.get_experiment(task.experiment_id)
                payment = await sync_to_async(Payment.objects.get)(task_id=task.id)

                if experiment.max_duration is None:
                    time_left_to_complete = "∞"
                else:
                    time_left_to_complete = timeuntil(
                        task.started_at + timedelta(minutes=experiment.max_duration),
                        datetime.now(timezone.utc),
                    )

                duration = ""
                if task.completed_at is not None and task.started_at is not None:
                    duration = naturaltime(
                        timeuntil(task.completed_at, task.started_at)
                    )
                elif task.failed_at is not None and task.started_at is not None:
                    duration = naturaltime(timeuntil(task.failed_at, task.started_at))

                failed_reason = ""
                if task.failed_reason != "":
                    for reason in ast.literal_eval(task.failed_reason):
                        reason_parts = re.findall(
                            r"[A-Z]?[a-z]+|[A-Z]+(?=[A-Z]|$)", reason
                        )
                        failed_reason += " ".join(reason_parts).capitalize() + "<br>"

                json_data["tasks"][task.id] = {
                    "id": task.id,
                    "started_at": formatted_datetime(task.started_at),
                    "completed_at": formatted_datetime(task.completed_at),
                    "time_left_to_complete": str(time_left_to_complete),
                    "failed_at": (
                        formatted_datetime(task.failed_at)
                        if task.failed_at is not None
                        else ""
                    ),
                    "failed_reason": failed_reason,
                    "duration": duration,
                    "payment": {
                        "amount": as_dollars(payment.amount),
                        "paid_at": formatted_datetime(payment.paid_at),
                        "received_at": formatted_datetime(payment.received_at),
                        "state": payment.state,
                    },
                    "state": task.state,
                    "base_payment": as_dollars(task.base_payment),
                    "bonus": as_dollars(task.bonus),
                    "total_payment_as_number": payment.amount,
                    "reset_at": formatted_datetime(task.reset_at),
                }
            if has_outstanding_payments is not None:
                json_data["has_outstanding_payments"] = has_outstanding_payments
                json_data["outstanding_payments_in_dollars"] = "{:,.2f}".format(
                    outstanding_payments_in_dollars
                )
                has_finished_non_test_task = await sync_to_async(
                    custom_user.has_finished_non_test_task
                )()
                json_data["has_finished_non_test_task"] = (
                    True if has_finished_non_test_task else False
                )
            if custom_user is not None:
                total_payments = as_dollars(
                    await sync_to_async(custom_user.total_payments)()
                )
            else:
                total_base_payments = as_dollars(
                    await sync_to_async(experiment.total_base_payments)()
                )
                total_bonuses = as_dollars(
                    await sync_to_async(experiment.total_bonuses)()
                )
                total_payments = as_dollars(
                    await sync_to_async(experiment.total_payments)()
                )
                json_data["total_base_payments"] = total_base_payments
                json_data["total_bonuses"] = total_bonuses
            json_data["total_payments"] = total_payments

            await self.send(text_data=json.dumps(json_data))
            await asyncio.sleep(2)

    async def receive(self, text_data):
        from accounts.models import CustomUser

        text_data_json = json.loads(text_data)
        custom_user_id = text_data_json["custom_user_id"]
        experiment_id = text_data_json.get("experiment_id")
        worker_id = text_data_json.get("worker_id")
        custom_user = await sync_to_async(CustomUser.objects.get)(pk=custom_user_id)
        has_outstanding_payments = None
        if worker_id is not None:
            worker = await sync_to_async(CustomUser.objects.get)(pk=worker_id)
            has_outstanding_payments = await sync_to_async(
                worker.has_outstanding_payments
            )()

        await login(
            self.scope, custom_user, backend="django.contrib.auth.backends.ModelBackend"
        )
        await database_sync_to_async(self.scope["session"].save)()
        await self.channel_layer.group_send(
            "admin_tasks_group",
            {
                "type": "send_admin_tasks",
                "experiment_id": experiment_id,
                "worker_id": worker_id,
                "has_outstanding_payments": has_outstanding_payments,
            },
        )


class WorkerTasksConsumer(AsyncWebsocketConsumer):
    @database_sync_to_async
    def get_experiment(self, experiment_id):
        from experiments.models import Experiment

        return get_object_or_404(Experiment, pk=experiment_id)

    @database_sync_to_async
    def get_tasks(self, custom_user_id):
        from experiments.models import Task

        return list(Task.objects.filter(worker_id=custom_user_id).all())

    @database_sync_to_async
    def get_outstanding_payments_in_dollars(self, worker_id):
        from accounts.models import CustomUser

        worker = CustomUser.objects.get(pk=worker_id)
        return worker.outstanding_payments_sum()

    async def connect(self):
        self.groupname = "worker_tasks_group"
        await self.channel_layer.group_add(
            self.groupname,
            self.channel_name,
        )
        await self.accept()

    async def disconnect(self, code):
        await self.channel_layer.group_discard(
            self.groupname,
            self.channel_name,
        )
        await super().disconnect(code)

    async def send_worker_tasks(self, event):
        from accounts.models import CustomUser

        custom_user = await sync_to_async(CustomUser.objects.get)(
            pk=event.get("custom_user_id")
        )
        has_outstanding_payments = None

        while True:
            json_data = {}

            await sync_to_async(custom_user.refresh_from_db)()
            tasks = await self.get_tasks(custom_user.id)
            outstanding_payments_in_dollars = (
                await self.get_outstanding_payments_in_dollars(custom_user.id)
            )
            has_outstanding_payments = (
                True if outstanding_payments_in_dollars > 0 else False
            )

            json_data = {}
            json_data["tasks"] = {}

            for task in tasks:
                experiment = await self.get_experiment(task.experiment_id)
                payment = await sync_to_async(Payment.objects.get)(task_id=task.id)
                started_at = naturaltime(task.started_at)
                completed_at = naturaltime(task.completed_at)
                if experiment.max_duration is None:
                    time_left_to_complete = "∞"
                else:
                    time_left_to_complete = timeuntil(
                        task.started_at + timedelta(minutes=experiment.max_duration),
                        datetime.now(timezone.utc),
                    )
                duration = ""
                if task.completed_at is not None and task.started_at is not None:
                    duration = naturaltime(
                        timeuntil(task.completed_at, task.started_at)
                    )
                elif task.failed_at is not None and task.started_at is not None:
                    duration = naturaltime(timeuntil(task.failed_at, task.started_at))

                if task.failed_at is not None:
                    failed_at = naturaltime(task.failed_at)
                failed_reason = ""
                if task.failed_reason != "":
                    for reason in ast.literal_eval(task.failed_reason):
                        reason_parts = re.findall(
                            r"[A-Z]?[a-z]+|[A-Z]+(?=[A-Z]|$)", reason
                        )
                        failed_reason += " ".join(reason_parts).capitalize() + "<br>"

                json_data["tasks"][task.id] = {
                    "id": task.id,
                    "started_at": (
                        str(started_at) if started_at is not None else started_at
                    ),
                    "completed_at": (
                        str(completed_at)
                        if task.state == "completed" and completed_at is not None
                        else completed_at
                    ),
                    "time_left_to_complete": str(time_left_to_complete),
                    "failed_at": failed_at if task.failed_at is not None else "",
                    "failed_reason": failed_reason,
                    "payment_done": payment.state == "paid",
                    "payment_received": payment.state == "received",
                    "duration": duration,
                    "payment": {
                        "amount": as_dollars(payment.amount),
                        "state": payment.state,
                        "is_paid": payment.state in ["paid", "received"],
                    },
                    "base_payment": as_dollars(task.base_payment),
                    "bonus": as_dollars(task.bonus),
                    "state": task.state,
                    "total_payment": as_dollars(task.base_payment + task.bonus),
                    "reset_at": formatted_datetime(task.reset_at),
                }
            if has_outstanding_payments is not None:
                json_data["has_outstanding_payments"] = has_outstanding_payments
                json_data["outstanding_payments_in_dollars"] = "{:,.2f}".format(
                    outstanding_payments_in_dollars
                )
                has_finished_non_test_task = await sync_to_async(
                    custom_user.has_finished_non_test_task
                )()
                json_data["has_finished_non_test_task"] = (
                    True if has_finished_non_test_task else False
                )

            json_data["participant_is_verified"] = await sync_to_async(
                custom_user.is_verified
            )()
            json_data["started_test_task"] = (
                await sync_to_async(custom_user.worker_tasks_count)() == 1
            )
            json_data["test_task_verified"] = custom_user.test_task_verified
            json_data["total_payments"] = as_dollars(
                await sync_to_async(custom_user.total_payments)()
            )

            await self.send(text_data=json.dumps(json_data))
            await asyncio.sleep(2)

    async def receive(self, text_data):
        from accounts.models import CustomUser

        text_data_json = json.loads(text_data)
        custom_user_id = text_data_json["custom_user_id"]
        custom_user = await sync_to_async(CustomUser.objects.get)(pk=custom_user_id)

        await login(
            self.scope, custom_user, backend="django.contrib.auth.backends.ModelBackend"
        )
        await database_sync_to_async(self.scope["session"].save)()
        await self.channel_layer.group_send(
            "worker_tasks_group",
            {
                "type": "send_worker_tasks",
                "custom_user_id": custom_user.id,
            },
        )


class AdminNotificationsConsumer(AsyncWebsocketConsumer):
    @database_sync_to_async
    def get_last_log_entry(self):
        from auditlog.models import LogEntry

        from accounts.models import CustomUser

        LogEntry.to_nat_lang = to_nat_lang

        staff_ids = [
            custom_user.id
            for custom_user in CustomUser.objects.filter(is_staff__exact=True)
        ]

        return (
            LogEntry.objects.filter(
                Q(object_repr__startswith="CustomUser ")
                # | Q(object_repr__startswith="Payment ")
                | Q(object_repr__startswith="Task ")
            )
            .exclude(actor_id__in=staff_ids)
            .order_by("id")
            .last()
        )

    def get_last_log_entries(self, n=5):
        from auditlog.models import LogEntry

        return list(LogEntry.objects.order_by("id")[:n])

    async def connect(self):
        self.groupname = "admin_notifications_group"
        await self.channel_layer.group_add(
            self.groupname,
            self.channel_name,
        )
        await self.accept()

    async def disconnect(self, code):
        await self.channel_layer.group_discard(
            self.groupname,
            self.channel_name,
        )
        await super().disconnect(code)

    async def send_admin_notifications(self, event):
        from accounts.models import CustomUser

        while True:
            json_data = {}
            actions = {0: "created", 1: "updated", 2: "deleted"}
            custom_user = None

            log_entry = await self.get_last_log_entry()
            if log_entry is None:
                return json_data

            action = actions[log_entry.action]

            if log_entry.actor_id is not None:
                custom_user = await sync_to_async(CustomUser.objects.get)(
                    pk=log_entry.actor_id
                )
            if action == "deleted":
                json_data[log_entry.id] = {
                    "action": action,
                    "nat_lang": await sync_to_async(log_entry.to_nat_lang)(
                        custom_user, action
                    ),
                    "timestamp": naturaltime(log_entry.timestamp),
                }
            elif action in ["created", "updated"]:
                cls = None
                obj = None
                try:
                    cls = getattr(
                        sys.modules[__name__], log_entry.object_repr.split(" ")[0]
                    )
                    try:
                        obj = await sync_to_async(cls.objects.get)(
                            pk=log_entry.object_pk
                        )
                        if custom_user is None:
                            custom_user_id = None
                            if isinstance(obj, CustomUser):
                                custom_user_id = obj.id
                            elif isinstance(obj, Task):
                                custom_user_id = obj.worker_id
                            custom_user = await sync_to_async(CustomUser.objects.get)(
                                pk=custom_user_id
                            )
                    except Exception:
                        pass
                    json_data[log_entry.id] = {
                        "action": action,
                        "nat_lang": str(
                            await sync_to_async(log_entry.to_nat_lang)(
                                custom_user, action, obj
                            )
                        ),
                        "timestamp": str(naturaltime(log_entry.timestamp)),
                    }
                except AttributeError as error:
                    logger.error(
                        f"Error getting class for log_entry {log_entry}: {error}"
                    )

            await self.send(text_data=json.dumps(json_data))
            await asyncio.sleep(2)

    async def receive(self, text_data):
        from accounts.models import CustomUser

        text_data_json = json.loads(text_data)
        custom_user_id = text_data_json["custom_user_id"]
        custom_user = await sync_to_async(CustomUser.objects.get)(pk=custom_user_id)

        await login(
            self.scope, custom_user, backend="django.contrib.auth.backends.ModelBackend"
        )
        await database_sync_to_async(self.scope["session"].save)()
        await self.channel_layer.group_send(
            "admin_notifications_group",
            {
                "type": "send_admin_notifications",
            },
        )


def to_nat_lang(self, custom_user, action, obj=None):
    changes = self.changes
    text = [custom_user.full_name()] if custom_user is not None else ["Admin"]

    if obj is None and action == "deleted":
        text.append("deleted failed initial test")
    elif isinstance(obj, CustomUser):
        text = [custom_user.full_name()]
        if action == "updated":
            if obj.deleted_at is None:
                if changes == {"test_task_verified": ["False", "True"]}:
                    text.append("passed initial test")
                else:
                    text.append("signed up")
            else:
                text.append("deleted their account")
    elif isinstance(obj, Task):
        text = [custom_user.full_name()]
        if action == "created":
            text.append("started")
        elif action == "updated":
            if changes["state"] == ["started", "completed"]:
                text.append("completed")
            elif changes["state"] == ["started", "not_completed"]:
                text.append("failed")
        elif action == "deleted":
            text.append(f"Admin deleted test task for {custom_user.full_name()}")
        text.append(f"task <i>{obj.experiment_title()}</i>")
    # elif isinstance(obj, Payment):
    #     if custom_user is not None:
    #         text = [custom_user.full_name()]
    #     else:
    #         text = ["Participant"]
    #     if action == "updated":
    #         if changes
    #         if changes["state"] == ["paid", "received"]:
    #             text.append("received payment for")
    #     elif action == "created":
    #         if changes["state"] == ["None", "unpaid"]:
    #             text.append("completed")
    #     text.append(f"task <i>{obj.task.experiment_title()}</i>")
    else:
        return ""

    text.append(
        f'(<span id="log-entry-naturaltime">{naturaltime(self.timestamp)}</span>)'
    )
    text = " ".join(text)
    if custom_user is None:
        return text
    return f'<a style="color: white;" href="/experiments/participants/{custom_user.id}/tasks">{text}</a>'


def as_dollars(value):
    if value is None or value == "":
        value = 0
    return "${:,.2f}".format(value)


def formatted_datetime(datetime):
    return (
        date_format(localtime(datetime), "DATETIME_FORMAT")
        if datetime is not None
        else ""
    )
