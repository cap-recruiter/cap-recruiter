import pytest
from allauth.account.models import EmailAddress
from django.contrib.auth import get_user_model
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By

from accounts.models import CustomUserDemographicInfo, GroupManager

CustomUser = get_user_model()


class LoginPagetest(LiveServerTestCase):
    def setUp(self):
        self.group_manager_custom_user = CustomUser(
            username="group_manager_1",
            email="gm_1@localhost",
            first_name="Gerd",
            last_name="Gott",
            is_group_manager=True,
        )
        self.group_manager_custom_user.set_password("some_password_gm1")
        self.group_manager_custom_user.save()
        self.group_manager_custom_user.group_manager_id = (
            self.group_manager_custom_user.id
        )
        self.group_manager_custom_user.save()

        self.group_manager = GroupManager.objects.create(
            code="capcode123456",
            user_id=self.group_manager_custom_user.id,
        )

        CustomUserDemographicInfo.objects.create(
            custom_user=self.group_manager_custom_user,
            country_code="DE",
        )

        self.worker_email = "worker_1@localhost"
        self.worker = CustomUser(
            username="worker_1",
            email=self.worker_email,
            first_name="Anna",
            last_name="Alberg",
            worker_id="AA123456",
            group_manager_id=self.group_manager_custom_user.id,
        )
        self.worker_password = "some_password_w1"
        self.worker.set_password(self.worker_password)
        self.worker.save()

        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-dev-shm-usage")
        self.driver = webdriver.Chrome(options=chrome_options)
        self.driver.get(self.live_server_url)

    def test_login_page_title(self):
        assert "CAP-Recruiter" in self.driver.title

    def test_failed_login(self):
        self._login(password="WRONG_PASSWORD")

        assert (
            "The email address and/or password you specified are not correct."
            in self.driver.page_source
        )

    def test_successful_login_unverified_account(self):
        self._login()

        assert "Available experiments" not in self.driver.page_source
        assert (
            "Please, follow the instructions to confirm your email address in the email which was sent to <i>worker_1@localhost</i> !"
            in self.driver.page_source
        )

    def test_successful_login_verified_account_no_test_task(self):
        EmailAddress.objects.create(
            email=self.worker.email, verified=True, primary=True, user_id=self.worker.id
        )

        self._login()

        assert "Available experiments" not in self.driver.page_source
        assert (
            "There are no experiments available currently." in self.driver.page_source
        )

    @pytest.mark.skip(reason="TODO")
    def test_successful_login_verified_account_available_test_task(self):
        EmailAddress.objects.create(
            email=self.worker.email, verified=True, primary=True, user_id=self.worker.id
        )

        self.worker.test_task_verified = False
        self.worker.save()

        self._login()

        assert "Initial test experiment" in self.driver.page_source
        assert (
            "Now, before starting real experiments, we need you to complete a test task first. This will only happen once."
            in self.driver.page_source
        )

    def test_successful_login_with_test_task(self):
        EmailAddress.objects.create(
            email=self.worker.email, verified=True, primary=True, user_id=self.worker.id
        )
        self.worker.test_task_verified = True
        self.worker.save()

        self._login()

        assert "Available experiments" in self.driver.page_source
        assert (
            "There are no experiments available currently." in self.driver.page_source
        )

    def test_successful_login_with_notice(self):
        EmailAddress.objects.create(
            email=self.worker.email, verified=True, primary=True, user_id=self.worker.id
        )
        self.worker.test_task_verified = True
        self.worker.save()
        self.group_manager.display_notice = True
        self.group_manager.notice = "Notice to be displayed."
        self.group_manager.save()

        self._login()

        assert "Available experiments" in self.driver.page_source
        assert "Notice to be displayed." in self.driver.page_source
        assert (
            "There are no experiments available currently." in self.driver.page_source
        )

    def _login(self, password=None):
        if password is None:
            password = self.worker_password

        user_login = self.driver.find_element(By.NAME, "login")
        user_password = self.driver.find_element(By.NAME, "password")
        user_login.send_keys(self.worker_email)
        user_password.send_keys(password)
        self.driver.find_element(By.XPATH, "//button[@type='submit']").click()
