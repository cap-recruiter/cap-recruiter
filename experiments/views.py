import csv
import io
import json
import logging
from datetime import datetime, timedelta
from statistics import median

from auditlog.models import LogEntry
from decouple import config
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from django.core.mail import send_mail
from django.db import transaction
from django.db.models import Count
from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse
from django.utils.text import slugify
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView, UpdateView
from django_fsm import TransitionNotAllowed

# from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

from accounts.forms import (
    GroupManagerCreationForm,
    GroupManagerUpdateForm,
    ParticipantUpdateForm,
)
from accounts.models import CustomUserDemographicInfo  # noqa: F401
from accounts.models import CustomUser, GroupManager, Message

from .forms import (
    EditableSettingsUpdateForm,
    ExperimentCreationForm,
    ExperimentUpdateForm,
)
from .models import EditableSettings, Experiment, Payment, Task
from .templatetags.tags import as_dollars

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


class ContextMixin(object):
    def get_context_data(self, request, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({"ws_protocol": config("WS_PROTOCOL")})
        context.update({"ws_host": config("WS_HOST")})

        if request.user.is_authenticated:
            context.update({"user_type": request.user.type})
        return context


# EditableSettings
class EditableSettingsUpdateView(
    ContextMixin, LoginRequiredMixin, SuccessMessageMixin, UpdateView
):
    template_name = "editable_settings/editable_settings_update_form.html"
    form_class = EditableSettingsUpdateForm
    model = EditableSettings

    def get_context_data(self, **kwargs):
        context = super().get_context_data(self.request, **kwargs)
        context["django_env"] = config("DJANGO_ENV")
        context["editable_settings"] = EditableSettings.load()
        context["menu_items"] = menu_items(self.request.user)
        return context

    def post(self, request, pk):
        show_log = self.request.POST.get("show_log", False)
        editable_settings = EditableSettings.load()
        editable_settings.show_log = show_log
        editable_settings.save()

        return redirect(reverse("home"))


# Experiment
class ExperimentsPageView(ContextMixin, LoginRequiredMixin, TemplateView):
    template_name = "experiments.html"

    def get_context_data(self, *args, **kwargs):
        context = super(ExperimentsPageView, self).get_context_data(
            self.request, *args, **kwargs
        )
        deployed_experiment_ids = Experiment.deployed_experiments()

        if self.request.user.is_authenticated:
            sort_by = self.request.GET.get("sort_by", "")
            sort_dir = self.request.GET.get("sort_dir", "")
            order_by = None

            if self.request.user.is_superuser:
                experiments = Experiment.objects.all()
                experiments = experiments.annotate(tasks_count=Count("tasks"))
                context["deployed_experiments"] = deployed_experiment_ids
                context["exists_deployed_experiment"] = (
                    True if deployed_experiment_ids else False
                )
                context["default_num_of_group_managers_to_display"] = 4
            elif self.request.user.is_group_manager:
                experiments = self.request.user.experiments()

            if self.request.GET.get("archived") == "true":
                experiments = experiments.filter(deleted_at__isnull=False)
            else:
                experiments = experiments.filter(deleted_at__isnull=True)

            if sort_by and sort_dir:
                order_by = f'{"-" if sort_dir == "desc" else ""}{sort_by}'
            if order_by is not None:
                context["experiments"] = experiments.order_by(order_by)
            else:
                experiments = experiments.order_by("-created_at")
                context["experiments"] = sorted(
                    experiments,
                    key=lambda e: (e.is_active_and_deployed()),
                    reverse=True,
                )
            context["menu_items"] = menu_items(self.request.user)
            context["django_env"] = config("DJANGO_ENV")
            context["custom_user_id"] = self.request.user.id

        return context


class ExperimentTasksPageView(ContextMixin, LoginRequiredMixin, TemplateView):
    template_name = "experiment_tasks.html"

    def get_context_data(self, pk, *args, **kwargs):
        context = super(ExperimentTasksPageView, self).get_context_data(
            self.request, *args, **kwargs
        )

        if self.request.user.is_authenticated:
            experiment = get_object_or_404(Experiment, pk=pk)
            tasks = experiment.tasks.all()
            tasks_completed = tasks.filter(completed_at__isnull=False)
            durations = [
                (task.completed_at - task.started_at) / timedelta(seconds=1)
                for task in tasks
                if task.completed_at is not None and task.started_at is not None
            ]
            minutes, seconds = (
                divmod(median(sorted(durations)), 60) if len(durations) > 0 else (0, 0)
            )
            tasks_count = tasks.count() if tasks.count() > 0 else 1

            if self.request.user.is_superuser or self.request.user.is_group_manager:
                context["tasks"] = tasks.order_by("-id")
                context["experiment"] = experiment
            context["menu_items"] = menu_items(self.request.user)
            context["django_env"] = config("DJANGO_ENV")
            context["custom_user_id"] = self.request.user.id
            context["deployed_experiments"] = Experiment.deployed_experiments()
            context["median_duration_min"] = int(minutes)
            context["median_duration_sec"] = int(seconds)
            context["percentage_completed"] = round(
                (tasks_completed.count() / tasks_count) * 100
            )

        return context


class ExperimentCreateView(ContextMixin, LoginRequiredMixin, CreateView):
    template_name_suffix = "_create_update_form"
    form_class = ExperimentCreationForm
    model = Experiment

    def get_context_data(self, **kwargs):
        context = super().get_context_data(self.request, **kwargs)
        context["available_group_managers"] = GroupManager.objects.all()
        context["django_env"] = config("DJANGO_ENV")
        context["menu_items"] = menu_items(self.request.user)
        context["custom_user_id"] = self.request.user.id
        context["page_title"] = "New Experiment"
        return context


class ExperimentUpdateView(ContextMixin, LoginRequiredMixin, UpdateView):
    template_name_suffix = "_create_update_form"
    form_class = ExperimentUpdateForm
    model = Experiment

    def get_context_data(self, **kwargs):
        context = super().get_context_data(self.request, **kwargs)
        context["available_group_managers"] = GroupManager.objects.all()
        context["django_env"] = config("DJANGO_ENV")
        context["menu_items"] = menu_items(self.request.user)
        context["custom_user_id"] = self.request.user.id
        context["page_title"] = "Edit Experiment"
        return context


class ExperimentDeleteView(LoginRequiredMixin, TemplateView):
    """
    This class handles HTTP GET requests for deleting an experiment and related Tasks.
    """

    def get(self, request, pk):
        tasks = Task.objects.filter(experiment_id=pk)
        experiment = get_object_or_404(Experiment, pk=pk)

        try:
            with transaction.atomic():
                if experiment is not None:
                    for task in tasks:
                        task.deleted_at = datetime.now()
                        task.save()
                    experiment.deleted_at = datetime.now()
                    experiment.save()

                    messages.success(
                        request,
                        f"Experiment with ID '{pk}' and related Tasks have been archived.",
                    )
            return HttpResponse(status=200)
        except Exception as e:
            error_msg = f"Error archiving experiment with ID '{pk}': {e}"
            logger.error(error_msg)
            return HttpResponse(error_msg, status=500)


class ExperimentDetailView(ContextMixin, LoginRequiredMixin, TemplateView):
    template_name = "experiment_detail.html"

    def get_context_data(self, pk, *args, **kwargs):
        experiment = get_object_or_404(Experiment, pk=pk)
        context = super().get_context_data(self.request, **kwargs)
        context["experiment"] = experiment
        context["django_env"] = config("DJANGO_ENV")
        context["page_title"] = "Experiment details"
        context["menu_items"] = menu_items(self.request.user)
        context["deployed_experiments"] = Experiment.deployed_experiments()
        context["custom_user_id"] = self.request.user.id
        return context


# GroupManager
class GroupManagerCreateView(ContextMixin, LoginRequiredMixin, CreateView):
    template_name = "group_managers/group_manager_create_form.html"
    form_class = GroupManagerCreationForm
    model = CustomUser

    def get_context_data(self, **kwargs):
        context = super().get_context_data(self.request, **kwargs)
        context["django_env"] = config("DJANGO_ENV")
        context["menu_items"] = menu_items(self.request.user)
        context["custom_user_id"] = self.request.user.id
        context["initial_test_experiments"] = Experiment.objects.filter(
            use_for_test_tasks=True, deleted_at=None
        )
        return context


class GroupManagerUpdateView(ContextMixin, LoginRequiredMixin, UpdateView):
    template_name = "group_managers/group_manager_update_form.html"
    form_class = GroupManagerUpdateForm
    model = CustomUser

    def get_context_data(self, **kwargs):
        context = super().get_context_data(self.request, **kwargs)
        context["django_env"] = config("DJANGO_ENV")
        context["menu_items"] = menu_items(self.request.user)
        context["custom_user_id"] = self.request.user.id
        context["initial_test_experiment_id"] = context[
            "customuser"
        ].groupmanager.initial_test_experiment_id
        context["group_manager_code"] = context["customuser"].group_manager_code
        context["initial_test_experiments"] = Experiment.objects.filter(
            use_for_test_tasks=True, deleted_at=None
        )
        return context

    def post(self, request, pk):
        group_manager = get_object_or_404(GroupManager, user_id=pk)
        custom_user = group_manager.user
        custom_user.first_name = request.POST.get("first_name")
        custom_user.last_name = request.POST.get("last_name")
        custom_user.email = request.POST.get("email")
        custom_user.save()
        demographic_info = group_manager.user.demographic_info
        demographic_info.country_code = request.POST.get("country_code")
        demographic_info.save()
        group_manager.name = request.POST.get("name")
        group_manager.display_notice = request.POST.get("display_notice") == "on"
        group_manager.notice = request.POST.get("notice")
        group_manager.consent = request.POST.get("consent")
        group_manager.enable_experiment_start_buttons = (
            request.POST.get("enable_experiment_start_buttons") == "on"
        )
        if request.POST.get("initial_test_experiment"):
            group_manager.initial_test_experiment_id = request.POST.get(
                "initial_test_experiment"
            )
        else:
            group_manager.initial_test_experiment_id = None
        group_manager.save()

        return redirect(reverse("experiments-group_managers"))


class GroupManagersPageView(ContextMixin, LoginRequiredMixin, TemplateView):
    template_name = "group_managers.html"

    def get_context_data(self, *args, **kwargs):
        context = super(GroupManagersPageView, self).get_context_data(
            self.request, *args, **kwargs
        )
        deployed_experiment_ids = Experiment.deployed_experiments()

        if self.request.user.is_authenticated:
            sort_by = self.request.GET.get("sort_by", "")
            sort_dir = self.request.GET.get("sort_dir", "")
            order_by = "-date_joined"
            if sort_by and sort_dir:
                order_by = f'{"-" if sort_dir == "desc" else ""}{sort_by}'
            sortable_extra_columns = [
                "country_name",
                "enable_experiment_start_buttons",
                "group_manager_code",
                "group_manager_consent",
                "group_manager_full_name",
                "group_manager_name",
                "participants_not_deleted_count",
            ]
            if self.request.user.is_superuser:
                group_managers = CustomUser.objects.filter(is_group_manager=True)
            if sort_by in sortable_extra_columns:
                reverse = True if sort_dir == "desc" else False
                group_managers = sorted(
                    group_managers,
                    key=lambda group_manager: getattr(group_manager, sort_by)(),
                    reverse=reverse,
                )
            else:
                group_managers = group_managers.order_by(order_by)
            context["group_managers"] = group_managers
            context["menu_items"] = menu_items(self.request.user)
            context["django_env"] = config("DJANGO_ENV")
            context["custom_user_id"] = self.request.user.id
            context["deployed_experiments"] = deployed_experiment_ids

        return context

    def menu_items(self):
        return menu_items(self.request.user)


class GroupManagerParticipantsPageView(ContextMixin, LoginRequiredMixin, TemplateView):
    template_name = "group_manager_participants.html"

    def get_context_data(self, pk, *args, **kwargs):
        context = super(GroupManagerParticipantsPageView, self).get_context_data(
            self.request, *args, **kwargs
        )
        group_manager = get_object_or_404(CustomUser, pk=pk)
        if self.request.user.is_authenticated and self.request.user.is_superuser:
            sort_by = self.request.GET.get("sort_by", "")
            sort_dir = self.request.GET.get("sort_dir", "")
            order_by = "-date_joined"
            if sort_by and sort_dir:
                order_by = f'{"-" if sort_dir == "desc" else ""}{sort_by}'
            sortable_extra_columns = [
                "outstanding_payments_sum",
                "country_name",
                "gender",
                "years_of_experience",
                "is_verified",
                "age",
                "worker_tasks_count",
            ]
            participants = group_manager.participants()

            if not self.request.GET.get("with_deleted") == "true":
                participants = participants.exclude(deleted_at__isnull=False)
            if sort_by in sortable_extra_columns:
                reverse = True if sort_dir == "desc" else False
                participants = sorted(
                    participants,
                    key=lambda participant: getattr(participant, sort_by)(),
                    reverse=reverse,
                )
            else:
                participants = participants.order_by(order_by)
            context["participants"] = participants
            context["group_manager"] = group_manager
            context["menu_items"] = menu_items(self.request.user)
            context["django_env"] = config("DJANGO_ENV")
            context["custom_user_id"] = self.request.user.id

        return context


class GroupManagerParticipantsExportView(LoginRequiredMixin, TemplateView, APIView):
    """
    This class exports all participants of a group as a CSV file.
    """

    def get(self, request, pk):
        group_manager_custom_user = get_object_or_404(CustomUser, pk=pk)
        participants = group_manager_custom_user.participants_not_deleted().order_by(
            "last_name"
        )

        response = HttpResponse(content_type="application/csv")
        response["Content-Disposition"] = (
            f"attachment; filename=group-manager_{group_manager_custom_user.group_manager_code()}_participants.csv"
        )

        writer = csv.writer(response, delimiter=",")
        header_row = [
            "First name",
            "Last name",
            "Recruitment ID",
            "Email",
            "Country",
            "Date of birth",
            "Age",
            "Gender",
            "Outstanding payments",
            "Nonresponsive",
        ]

        writer.writerow(header_row)

        for participant in participants:
            data_row = [
                participant.first_name,
                participant.last_name,
                participant.worker_id,
                participant.email,
                participant.country_name(),
                participant.date_of_birth(),
                participant.age(),
                participant.gender(),
                participant.outstanding_payments_sum(),
                "Yes" if participant.nonresponsive else "",
            ]
            writer.writerow(data_row)

        return response


class GroupManagerMessageComposeView(ContextMixin, LoginRequiredMixin, TemplateView):
    """
    This class handles HTTP GET requests for composing an email that can be sent to all members of a group.
    """

    template_name = "send_group_message.html"

    def get_context_data(self, pk, *args, **kwargs):
        context = super(GroupManagerMessageComposeView, self).get_context_data(
            self.request, *args, **kwargs
        )

        current_user = self.request.user
        if current_user.is_authenticated:
            if current_user.is_superuser or current_user.is_group_manager:
                group_manager = get_object_or_404(CustomUser, pk=pk)
                context["group_manager"] = group_manager
                context["recipients"] = group_manager.participants_not_deleted()
                context["messages_"] = group_manager.messages.order_by("-id")
                context["menu_items"] = menu_items(self.request.user)
                context["django_env"] = config("DJANGO_ENV")

                return context


class GroupManagerMessageSendView(TemplateView, LoginRequiredMixin, APIView):
    """
    This class handles HTTP POST requests for sending emails to all members of a group.
    """

    def post(self, request):
        request_data = request.POST
        subject = request_data["subject"]
        message_text = request_data["message_text"]
        group_manager = get_object_or_404(
            CustomUser, pk=request_data["group_manager_id"]
        )
        recipient_ids = request_data.getlist("recipient_ids[]") + [group_manager.id]
        recipients_count = len(recipient_ids) - 1
        logger.info(
            f"Sending emails to {recipients_count} members of group '{group_manager.group_manager_name()}'."
        )

        Message.objects.create(
            subject=subject,
            text=message_text,
            recipient_ids=recipient_ids,
            sender_id=group_manager.id,
            send_at=datetime.now(),
        )

        for recipient_id in recipient_ids:
            recipient = get_object_or_404(CustomUser, pk=recipient_id)
            send_mail(
                request_data["subject"],
                request_data["message_text"],
                "cap-experiments@ae.mpg.de",
                [recipient.email],
                fail_silently=False,
            )
            logger.info(f"Email sent to {recipient.email}.")
        messages.success(
            request,
            f"Email has been sent to {recipients_count} members of group '{group_manager.groupmanager.name}'.",
        )

        return redirect(reverse("experiments-group_managers"))


# HomePage
class HomePageView(ContextMixin, LoginRequiredMixin, TemplateView):
    template_name = "home.html"

    def get_context_data(self, *args, **kwargs):
        context = super(HomePageView, self).get_context_data(
            self.request, *args, **kwargs
        )
        deployed_experiment_ids = Experiment.deployed_experiments()

        if self.request.user.is_authenticated:
            if self.request.user.worker_id:
                if self.request.user.test_task_verified:
                    context["batched_experiments"] = (
                        self.request.user.available_experiments(deployed_experiment_ids)
                    )
                else:
                    test_tasks_experiment = self.request.user.initial_test_experiment()
                    if (
                        test_tasks_experiment
                        and test_tasks_experiment.id in deployed_experiment_ids
                    ):
                        context["test_tasks_experiment"] = test_tasks_experiment
                context["deployed_experiments"] = deployed_experiment_ids
                context["tasks"] = self.request.user.worker_tasks().order_by("-id")
                context["group_manager_code"] = self.request.user.group_manager_code()
                context["worker_id"] = self.request.user.worker_id
            elif self.request.user.is_group_manager:
                experiments = self.request.user.experiments()
                if self.request.GET.get("archived") == "true":
                    experiments = experiments.filter(deleted_at__isnull=False)
                context["experiments"] = experiments.order_by("-created_at")
            elif self.request.user.is_superuser:
                sort_by = self.request.GET.get("sort_by", "")
                sort_dir = self.request.GET.get("sort_dir", "")
                order_by = None

                context["group_managers"] = CustomUser.objects.filter(
                    is_group_manager=True
                )
                experiments = Experiment.objects.all()
                if self.request.GET.get("archived") == "true":
                    experiments = experiments.filter(deleted_at__isnull=False)

                if sort_by and sort_dir:
                    order_by = f'{"-" if sort_dir == "desc" else ""}{sort_by}'
                if order_by is not None:
                    context["experiments"] = experiments.order_by(order_by)
                else:
                    experiments = experiments.order_by("-created_at")
                    context["experiments"] = sorted(
                        experiments,
                        key=lambda e: (e.is_active_and_deployed()),
                        reverse=True,
                    )
                context["deployed_experiments"] = deployed_experiment_ids
                context["exists_deployed_experiment"] = (
                    True if deployed_experiment_ids else False
                )
                context["default_num_of_group_managers_to_display"] = 4
            context["menu_items"] = menu_items(self.request.user)
            context["recruiter"] = config("RECRUITER")
            context["django_env"] = config("DJANGO_ENV")
            context["custom_user_id"] = self.request.user.id

        return context

    def menu_items(self):
        return menu_items(self.request.user)


# Task
class TasksPageView(ContextMixin, LoginRequiredMixin, TemplateView):
    template_name = "tasks.html"

    def get_context_data(self, *args, **kwargs):
        context = super(TasksPageView, self).get_context_data(
            self.request, *args, **kwargs
        )
        logger.info("Getting deployed_experiments...")
        deployed_experiment_ids = Experiment.deployed_experiments()

        if self.request.user.is_authenticated:
            if self.request.user.worker_id:
                if self.request.user.test_task_verified:
                    context["batched_experiments"] = (
                        self.request.user.available_experiments(deployed_experiment_ids)
                    )
                else:
                    test_tasks_experiment = self.request.user.initial_test_experiment()
                    if (
                        test_tasks_experiment
                        and test_tasks_experiment.id in deployed_experiment_ids
                    ):
                        context["test_tasks_experiment"] = test_tasks_experiment
                context["deployed_experiments"] = deployed_experiment_ids
                context["tasks"] = self.request.user.worker_tasks().order_by("-id")
                context["group_manager_code"] = self.request.user.group_manager_code()
                context["worker_id"] = self.request.user.worker_id
                context["django_env"] = config("DJANGO_ENV")
                context["custom_user_id"] = self.request.user.id
            if self.request.user.is_group_manager:
                context["tasks"] = self.request.user.group_manager_tasks().order_by(
                    "-id"
                )
                context["group_manager_code"] = self.request.user.group_manager_code()
            context["menu_items"] = menu_items(self.request.user)
            context["django_env"] = config("DJANGO_ENV")

        return context

    def menu_items(self):
        return menu_items(self.request.user)


class TasksFailView(TemplateView, APIView):
    """
    This class handles HTTP POST requests when the participant has failed an experiment.
    """

    # permission_classes = (IsAuthenticated,)

    def post(self, request):
        request_data = json.loads(request.body)
        self.task = get_object_or_404(Task, assignment_id=request_data["assignmentId"])

        try:
            with transaction.atomic():
                self.task.fail()
                self.task.failed_at = datetime.now()
                self.task.base_payment = request_data["basePayment"]
                self.task.bonus = request_data["bonus"]
                self.task.failed_reason = request_data["failed_reason"]
                self.task.save()
                self.task.payment.amount = self.task.base_payment + self.task.bonus
                self.task.payment.save()
        except TransitionNotAllowed as exception:
            logger.error(exception)
            return JsonResponse(
                {
                    "result": "error",
                    "type": type(exception).__name__,
                    "message": str(exception),
                    "taskState": self.task.state,
                    "taskId": self.task.id,
                }
            )
        self.send_task_failed_email()

        return JsonResponse({"result": "success"})

    def send_task_failed_email(self):
        if self.task.group_manager.consent == "princeton":
            body = f"""Dear {self.task.participant_name()},

Thanks for participating in our experiment. Unfortunately, the experiment ended prematurely.
See below the experiment and payment information:

    {self.task.experiment_title()}
    Assignment ID: {self.task.assignment_id}
    Total pay: {as_dollars(self.task.total_payment())}
    Task ID: {self.task.id}

A member of our team will process your payment. You will receive this payment via Zelle in the coming month.
Please log in to your CAP-Recruiter account for further information about the payment system and your payment history.

If you have any questions, please send an email cocosci-lab@princeton.edu.


Kind Regards,

Computational Cognitive Science Lab
Princeton Department of Psychology

https://cocosci.princeton.edu/
"""
        else:
            body = f"""Dear {self.task.participant_name()},

Thanks for participating in our experiment. Unfortunately, the experiment ended prematurely.
See below the experiment and payment information:

    {self.task.experiment_title()}
    Assignment ID: {self.task.assignment_id}
    Total pay: {as_dollars(self.task.total_payment())}
    Task ID: {self.task.id}

A member of our team will process your payment. You will receive this payment via Wise in the coming month.
Please log in to your CAP-Recruiter account for further information about the payment system and your payment history.

If you have any questions, please send an email to cap-information@ae.mpg.de.


Kind Regards,

Research Group Computational Auditory Perception
Max Planck Institute for Empirical Aesthetics

https://www.aesthetics.mpg.de/en/research/research-group-computational-auditory-perception.html
"""
        send_mail(
            "Experiment Completed – CAP-Recruiter",
            body,
            "cap-information@ae.mpg.de",
            [self.task.participant_email()],
            fail_silently=False,
        )
        logger.info(
            f"Experiment failed. Email sent to participant with assignment_id '{self.task.assignment_id}'!"
        )


class TasksCompleteView(TemplateView, APIView):
    """
    This class handles HTTP POST requests when the participant has finished an experiment.
    """

    # permission_classes = (IsAuthenticated,)

    def post(self, request):
        request_data = json.loads(request.body)
        self.task = get_object_or_404(Task, assignment_id=request_data["assignmentId"])

        try:
            with transaction.atomic():
                self.task.complete()
                self.task.completed_at = datetime.now()
                self.task.base_payment = request_data["basePayment"]
                self.task.bonus = request_data["bonus"]
                self.task.save()
                self.task.payment.amount = self.task.base_payment + self.task.bonus
                self.task.payment.save()
                worker = self.task.worker
                if not worker.test_task_verified:
                    worker.test_task_verified = True
                    worker.save()
        except TransitionNotAllowed as exception:
            logger.error(exception)
            return JsonResponse(
                {
                    "result": "error",
                    "type": type(exception).__name__,
                    "message": str(exception),
                    "taskState": self.task.state,
                    "taskId": self.task.id,
                }
            )
        self.send_task_completed_email()

        return JsonResponse({"result": "success"})

    def send_task_completed_email(self):
        if self.task.group_manager.consent == "princeton":
            body = f"""Dear {self.task.participant_name()},

Thanks for participating in our experiment. See below the experiment and payment information:

    {self.task.experiment_title()}
    Assignment ID: {self.task.assignment_id}
    Total pay: {as_dollars(self.task.total_payment())}
    Task ID: {self.task.id}

A member of our team will process your payment. You will receive this payment via Zelle in the coming month.
Please log in to your CAP-Recruiter account for further information about the payment system and your payment history.

If you have any questions, please send an email to cocosci-lab@princeton.edu.


Kind Regards,

Computational Cognitive Science Lab
Princeton Department of Psychology

https://cocosci.princeton.edu/
"""
        else:
            body = f"""Dear {self.task.participant_name()},

Thanks for participating in our experiment. See below the experiment and payment information:

    {self.task.experiment_title()}
    Assignment ID: {self.task.assignment_id}
    Total pay: {as_dollars(self.task.total_payment())}
    Task ID: {self.task.id}

A member of our team will process your payment. You will receive this payment via Wise in the coming month.
Please log in to your CAP-Recruiter account for further information about the payment system and your payment history.

If you have any questions, please send an email to cap-information@ae.mpg.de.


Kind Regards,

Research Group Computational Auditory Perception
Max Planck Institute for Empirical Aesthetics

https://www.aesthetics.mpg.de/en/research/research-group-computational-auditory-perception.html
"""
        send_mail(
            "Experiment Completed – CAP-Recruiter",
            body,
            "cap-information@ae.mpg.de",
            [self.task.participant_email()],
            fail_silently=False,
        )
        logger.info(
            f"Experiment completed. Email sent to participant with assignment_id '{self.task.assignment_id}'!"
        )


class TaskResetView(LoginRequiredMixin, TemplateView):
    """
    This class handles HTTP GET requests for resetting a task.
    """

    def get(self, request, pk):
        task = get_object_or_404(Task, pk=pk)
        if task is not None:
            participant_id = task.worker.id
            task.reset()
            task.reset_at = datetime.now()
            task.save()
            messages.success(request, "Task has been reset.")
        else:
            messages.warning(request, "There was an error resetting the task.")

        return redirect(
            reverse("experiments-participant-tasks", args=(participant_id,))
        )


class TasksExportView(LoginRequiredMixin, TemplateView, APIView):
    """
    This class exports all tasks of an experiment having state 'completed' as a CSV file.
    """

    def get(self, request, pk):
        tasks = Task.objects.filter(experiment_id=pk)
        experiment = get_object_or_404(Experiment, pk=pk)

        response = HttpResponse(content_type="application/csv")
        response["Content-Disposition"] = (
            f"attachment; filename={slugify(experiment.title)}_tasks_completed.csv"
        )

        writer = csv.writer(response, delimiter=";")
        header_row = [
            "experiment_title",
            "task_id",
            "assignment_id",
            "worker_id",
            "group_manager_name",
            "group_manager_code",
            "base_payment",
            "bonus",
            "comment",
            "state",
        ]

        writer.writerow(header_row)

        for task in tasks:
            data_row = [
                task.experiment_title(),
                task.id,
                task.assignment_id,
                task.participant_worker_id(),
                task.group_manager_name(),
                task.group_manager_code(),
                task.base_payment,
                task.bonus,
                task.comment,
                task.state,
            ]
            writer.writerow(data_row)

        return response


class TasksImportView(LoginRequiredMixin, TemplateView, APIView):
    def post(self, request):
        csv_file = request.FILES.get("csv_file")
        experiment_id = request.POST.get("experiment_id")

        # save file
        default_storage.save(
            f"experiments/data/tasks_csv_imports/{csv_file.name}",
            ContentFile(csv_file.read()),
        )
        csv_file.seek(0)
        io_string = io.StringIO(csv_file.read().decode("UTF-8"))
        next(io_string)  # skip header

        num_of_updated_tasks = 0
        for row in csv.reader(io_string, delimiter=";"):
            task = get_object_or_404(Task, pk=row[1])
            if row[9] == "payment_done" and task.state == "completed":
                task.payment.pay()
                task.payment.paid_at = datetime.now()
                task.payment.save()
                num_of_updated_tasks += 1

        if num_of_updated_tasks > 0:
            messages.success(request, f"Updated {num_of_updated_tasks} Tasks.")
        elif num_of_updated_tasks == 0:
            messages.warning(request, "No Tasks were updated.")

        return redirect(reverse("experiments-experiment-tasks", args=(experiment_id,)))


class TasksPayView(LoginRequiredMixin, TemplateView):
    """
    This class handles HTTP GET requests for setting a Payment's state to 'paid'.
    """

    def get(self, request, pk):
        task = get_object_or_404(Task, pk=pk)
        task.payment.pay()
        task.payment.paid_at = datetime.now()
        task.payment.save()
        redirect_to = request.GET.get("redirect_to")
        if redirect_to == "experiment":
            return redirect(f"/experiments/{task.experiment.id}/tasks")
        elif redirect_to == "participant":
            return redirect(f"/experiments/participants/{task.worker_id}/tasks")


class TasksUpdateBonusView(LoginRequiredMixin, APIView):
    """
    This class handles HTTP POST requests for updating a Tasks's bonus.
    """

    def post(self, request, pk):
        task = get_object_or_404(Task, pk=pk)
        bonus = request.POST.get("bonus")
        if bonus is not None:
            task.bonus = bonus
            task.payment.amount = round(task.base_payment + float(task.bonus), 2)
            task.payment.save()
            task.save()

        return JsonResponse({"task_id": task.id}, status=200)


class TasksFinalizeView(LoginRequiredMixin, TemplateView):
    """
    This class handles HTTP GET requests for setting a Tasks's state to 'payment_done'.
    """

    def get(self, request, pk):
        task = get_object_or_404(Task, pk=pk)
        task.payment.receive()
        task.payment.received_at = datetime.now()
        task.payment.save()
        return redirect(reverse("experiments-tasks"))


class TasksDeleteTestTaskView(LoginRequiredMixin, TemplateView):
    """
    This class handles HTTP GET requests for deleting a user's test Task.
    """

    def get(self, request):
        task = request.user.worker_test_task()
        if task is not None and not request.user.test_task_verified:
            task.delete()
            messages.success(request, "The test Task has been reset.")
        else:
            messages.warning(
                request, "The test Task has already been completed successfully."
            )

        return redirect(reverse("home"))


class TasksStartView(LoginRequiredMixin, TemplateView):
    """
    This class handles HTTP GET requests when the participant clicks the link to start an experiment.
    """

    def get(self, request):
        experiment_id = request.GET.get("experimentId")
        assignment_id = request.GET.get("assignmentId")
        batch = request.GET.get("batch")
        group_manager = GroupManager.objects.filter(
            code=request.GET.get("groupManagerCode")
        ).first()
        worker_id = self.request.user.id

        task = Task.objects.create(
            experiment_id=experiment_id,
            worker_id=worker_id,
            group_manager_id=group_manager.id,
            assignment_id=assignment_id,
            batch=batch,
        )
        Payment.objects.create(
            task_id=task.id,
            worker_id=worker_id,
        )

        return JsonResponse({"task_id": task.id}, status=200)


class TasksUpdateCommentView(LoginRequiredMixin, TemplateView):
    """
    This class handles HTTP POST requests to update the comment field of a Task.
    """

    def post(self, request, pk):
        task = get_object_or_404(Task, pk=pk)
        task.comment = request.POST.get("comment")
        task.save()

        return HttpResponse(status=204)


# LogEntry
class LogEntriesPageView(ContextMixin, LoginRequiredMixin, TemplateView):
    template_name = "log_entries.html"

    def get_context_data(self, *args, **kwargs):
        context = super(LogEntriesPageView, self).get_context_data(
            self.request, *args, **kwargs
        )
        if self.request.user.is_authenticated:
            if self.request.user.is_superuser:
                log_entries = LogEntry.objects.all().order_by("-id")
                context["log_entries"] = list(
                    filter(
                        lambda item: item is not None,
                        map(self.display_log_entries, log_entries),
                    )
                )
            context["menu_items"] = menu_items(self.request.user)
            context["django_env"] = config("DJANGO_ENV")
            context["custom_user_id"] = self.request.user.id

        return context

    def display_log_entries(self, log_entry):
        import sys

        obj = None
        actions = {0: "Created", 1: "Updated", 2: "Deleted"}
        try:
            action = actions[log_entry.action]
            change_items = log_entry.changes
            class_name = log_entry.object_repr.split(" ")[0]
            cls = getattr(sys.modules[__name__], class_name)
            try:
                obj = cls.objects.get(pk=log_entry.object_pk)
            except Exception as e:
                logger.warning(f"Could not get object {log_entry}: {e}")

            custom_user = None

            try:
                custom_user = CustomUser.objects.get(pk=log_entry.actor_id)
            except Exception:
                if isinstance(obj, CustomUserDemographicInfo):
                    custom_user = CustomUser.objects.get(
                        worker_id=obj.custom_user.worker_id
                    )
                elif isinstance(obj, Task):
                    custom_user = CustomUser.objects.get(worker_id=obj.worker.worker_id)

            return {
                "id": log_entry.id,
                "action": (
                    "Deleted" if change_items.get("deleted_at") is not None else action
                ),
                "type": f"{cls.__name__} ({log_entry.object_pk})",
                "changes": change_items,
                "timestamp": log_entry.timestamp,
                "custom_user": custom_user,
            }
        except Exception as error:
            error_msg = f"Error getting log entries: {error}"
            logger.error(error_msg)

    def menu_items(self):
        return menu_items(self.request.user)


# Participant
class ParticipantsPageView(ContextMixin, LoginRequiredMixin, TemplateView):
    template_name = "participants.html"

    def get_context_data(self, *args, **kwargs):
        context = super(ParticipantsPageView, self).get_context_data(
            self.request, *args, **kwargs
        )
        if self.request.user.is_authenticated:
            sort_by = self.request.GET.get("sort_by", "")
            sort_dir = self.request.GET.get("sort_dir", "")
            order_by = "-date_joined"
            if sort_by and sort_dir:
                order_by = f'{"-" if sort_dir == "desc" else ""}{sort_by}'
            sortable_extra_columns = [
                "outstanding_payments_sum",
                "country_name",
                "is_verified",
                "worker_tasks_count",
                "group_manager_name",
            ]
            if self.request.user.is_group_manager:
                context["participants"] = (
                    self.request.user.participants()
                    .exclude(deleted_at__isnull=False)
                    .order_by(order_by)
                )
            if self.request.user.is_superuser:
                participants = CustomUser.objects.exclude(is_superuser=True).exclude(
                    is_group_manager=True
                )
                if not self.request.GET.get("with_deleted") == "true":
                    participants = participants.exclude(deleted_at__isnull=False)
                if sort_by in sortable_extra_columns:
                    reverse = True if sort_dir == "desc" else False
                    participants = sorted(
                        participants,
                        key=lambda participant: getattr(participant, sort_by)(),
                        reverse=reverse,
                    )
                else:
                    participants = participants.order_by(order_by)
                context["participants"] = participants

            context["menu_items"] = menu_items(self.request.user)
            context["django_env"] = config("DJANGO_ENV")
            context["custom_user_id"] = self.request.user.id

        return context

    def menu_items(self):
        return menu_items(self.request.user)


class ParticipantTasksPageView(ContextMixin, LoginRequiredMixin, TemplateView):
    template_name = "participant_tasks.html"

    def get_context_data(self, pk, *args, **kwargs):
        context = super(ParticipantTasksPageView, self).get_context_data(
            self.request, *args, **kwargs
        )

        participant = get_object_or_404(CustomUser, pk=pk)

        if self.request.user.is_authenticated:
            if self.request.user.is_superuser or self.request.user.is_group_manager:
                context["tasks"] = participant.tasks.order_by("-id")
                context["participant"] = participant
            context["menu_items"] = menu_items(self.request.user)
            context["django_env"] = config("DJANGO_ENV")
            context["custom_user_id"] = self.request.user.id
            context["deployed_experiments"] = Experiment.deployed_experiments()

        return context


class ParticipantTasksPaySumPageView(LoginRequiredMixin, TemplateView):
    """
    This class handles HTTP GET requests for setting all Payments of a participant to 'paid'.
    """

    def get(self, request, pk):
        participant = get_object_or_404(CustomUser, pk=pk)
        for task in participant.outstanding_payments():
            task.payment.pay()
            task.payment.paid_at = datetime.now()
            task.payment.save()

        return redirect(f"/experiments/participants/{pk}/tasks")


class ParticipantUpdateView(ContextMixin, LoginRequiredMixin, UpdateView):
    template_name = "participants/participant_update_form.html"
    form_class = ParticipantUpdateForm
    model = CustomUser

    def get_context_data(self, **kwargs):
        context = super().get_context_data(self.request, **kwargs)
        context["django_env"] = config("DJANGO_ENV")
        context["menu_items"] = menu_items(self.request.user)
        context["custom_user_id"] = self.request.user.id
        return context

    def post(self, request, pk):
        custom_user = get_object_or_404(CustomUser, pk=pk)
        custom_user.first_name = request.POST.get("first_name")
        custom_user.last_name = request.POST.get("last_name")
        custom_user.nonresponsive = request.POST.get("nonresponsive") == "on"
        custom_user.save()
        demographic_info = custom_user.demographic_info
        demographic_info.country_code = request.POST.get("country_code")
        demographic_info.gender = request.POST.get("gender")
        demographic_info.date_of_birth = f'{request.POST.get("date_of_birth_year")}-{request.POST.get("date_of_birth_month").zfill(2)}-{request.POST.get("date_of_birth_day").zfill(2)}'
        demographic_info.years_of_experience = request.POST.get("years_of_experience")
        demographic_info.save()

        return redirect(
            reverse(
                "experiments-group_manager-participants",
                args=(custom_user.group_manager_id,),
            )
        )


def menu_items(user):
    editable_settings = EditableSettings.load()

    return {
        "superuser": [
            {"url": "/experiments/experiments", "title": "Experiments", "show": True},
            {
                "url": "/experiments/group_managers",
                "title": "Groups",
                "show": True,
            },
            {"url": "/experiments/participants", "title": "Participants", "show": True},
            {
                "url": "/experiments/log_entries",
                "title": "Log",
                "show": editable_settings.show_log,
            },
            {
                "url": "/experiments/editable_settings/1/update",
                "title": "Settings",
                "show": True,
            },
        ],
        "group_manager": [
            {"url": "/experiments/experiments", "title": "Experiments", "show": True},
            {"url": "/experiments/participants", "title": "Participants", "show": True},
            {"url": "/experiments/tasks", "title": "Tasks", "show": True},
        ],
        "worker": [
            {"url": "/experiments/tasks", "title": "Experiment history", "show": True},
            {
                "url": "/experiments/consent_info",
                "title": "Consent information",
                "show": True,
            },
        ],
    }[user.type()]
