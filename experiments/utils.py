from urllib.parse import ParseResult, urlparse


def get_scheme_and_netloc(url):
    parse_result = urlparse(url)
    return ParseResult(
        scheme=parse_result.scheme,
        netloc=parse_result.netloc,
        path="",
        params=None,
        query=None,
        fragment=None,
    ).geturl()
