import os
import subprocess
from platform import python_version

import django
from channels import __version__ as channels_version
from decouple import config
from django import template

register = template.Library()


@register.simple_tag
def get_django_version():
    return django.get_version()


@register.simple_tag
def get_channels_version():
    return channels_version


@register.simple_tag
def get_python_version():
    return python_version()


@register.simple_tag
def git_info():
    git_dir = os.path.dirname(f"longgold_db.settings.{config('DJANGO_ENV')}.BASE_DIR")

    try:
        head = subprocess.Popen(
            'git log -1 --pretty=format:"%cd (%h)" --date=local'.format(git_dir),
            shell=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
        )
        version = head.stdout.readline().strip().decode("utf-8")
        head = subprocess.Popen(
            "git describe --tags --abbrev=0".format(git_dir),
            shell=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
        )
        last_tag = head.stdout.readline().strip().decode("utf-8")
        git_info_string = f"<b>{last_tag}</b> | Last update: {version}"
    except BaseException:
        git_info_string = "Error getting git info"

    return git_info_string


@register.filter(name="add")
def add(value, arg):
    if value is None or arg is None:
        return None
    return value + arg


@register.filter(name="as_dollars")
def as_dollars(value):
    if value is None or value == "":
        value = 0
    return "${:,.2f}".format(value)


@register.filter(name="ids")
def ids(objects):
    return [o.id for o in objects]


@register.filter
def format_sort_title(str):
    return str.replace("demographic_info_", "").replace("_", " ")


@register.simple_tag
def active_and_deployed_experiment(experiment, deployed_experiment_ids):
    return experiment.is_active() and experiment.id in deployed_experiment_ids


@register.simple_tag
def active_experiment(experiment):
    return experiment.is_active()


@register.simple_tag
def deployed_experiment(experiment, deployed_experiment_ids):
    return experiment.id in deployed_experiment_ids


@register.simple_tag
def initial_test_experiment(experiment):
    return experiment.use_for_test_tasks


@register.simple_tag
def sort_experiments(experiments):
    # TODO additionally sort by datetime.strptime(str(e.created_at)[:19], '%Y-%m-%d %H:%M:%S'))
    return sorted(experiments, key=lambda e: (e.is_active_and_deployed()), reverse=True)


@register.simple_tag
def active_menu_item(menu_item_url, request_path):
    return (
        (
            menu_item_url == "/experiments/experiments"
            or menu_item_url == "/experiments/tasks"
        )
        and request_path == "/experiments/"
        or menu_item_url == request_path
        and menu_item_url != "/"
        or menu_item_url.split("/")[-1] == request_path.split("/")[-3]
    )
