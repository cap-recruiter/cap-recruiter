import uuid

from django import template

register = template.Library()


@register.simple_tag
def assignment_id():
    return str(uuid.uuid4()).split("-")[0]
