from django.urls import path

from .consumers import (
    AdminExperimentsConsumer,
    AdminNotificationsConsumer,
    AdminParticipantsConsumer,
    AdminTasksConsumer,
    WorkerTasksConsumer,
)

ws_urlpatterns = [
    path("wss/admin_experiments/", AdminExperimentsConsumer.as_asgi()),
    path("wss/admin_notifications/", AdminNotificationsConsumer.as_asgi()),
    path("wss/admin_participants/", AdminParticipantsConsumer.as_asgi()),
    path("wss/admin_tasks/", AdminTasksConsumer.as_asgi()),
    path("wss/worker_tasks/", WorkerTasksConsumer.as_asgi()),
]
