import logging
from datetime import datetime

from celery import shared_task
from channels.layers import get_channel_layer

from .models import Experiment

channel_layer = get_channel_layer()

logger = logging.getLogger(__name__)


@shared_task(name="check_experiment_duration_task")
def check_experiment_duration():
    for experiment in Experiment.objects.all():
        if not experiment.has_expired():
            for task in experiment.tasks.filter(state="started"):
                if task.max_duration_exceeded():
                    logger.info(
                        f"Failing Task '{task.id}' due to exceeded maximum duration."
                    )
                    task.fail()
                    task.failed_at = datetime.now()
                    task.failed_reason = ["Timeout reached"]
                    task.save()


def as_dollars(value):
    if value is None or value == "":
        value = 0
    return "${:,.2f}".format(value)
