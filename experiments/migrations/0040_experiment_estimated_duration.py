# Generated by Django 4.1.2 on 2022-12-01 00:38

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("experiments", "0039_auto_20221107_2122"),
    ]

    operations = [
        migrations.AddField(
            model_name="experiment",
            name="estimated_duration",
            field=models.IntegerField(default=0),
        ),
    ]
