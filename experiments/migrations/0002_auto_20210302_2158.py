# Generated by Django 3.1.7 on 2021-03-02 21:58

import django_fsm
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("experiments", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="hit",
            name="state",
            field=django_fsm.FSMField(default="new", max_length=50, protected=True),
        ),
    ]
