# Generated by Django 4.1.2 on 2022-10-27 22:00

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("accounts", "0014_rename_test_hit_verified_customuser_test_task_verified"),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("experiments", "0031_remove_hit_hit_id"),
    ]

    operations = [
        migrations.RenameModel("HIT", "Task"),
        migrations.RenameField(
            model_name="experiment",
            old_name="use_for_test_hits",
            new_name="use_for_test_tasks",
        ),
    ]
    atomic = False
