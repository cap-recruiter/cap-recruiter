# Generated by Django 3.1.7 on 2021-03-04 02:47

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("experiments", "0004_auto_20210304_0244"),
    ]

    operations = [
        migrations.AddField(
            model_name="hit",
            name="hit_id",
            field=models.CharField(db_index=True, default="", max_length=200),
            preserve_default=False,
        ),
    ]
