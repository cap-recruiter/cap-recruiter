# CAP Recruiter – Server installation
The following installation instructions apply to Ubuntu 24.04 LTS (Noble Numbat) server. They address setting up a production instance of CAP Recruiter using `cap-recruiter.cap-experiments.com` as the domain the app is deployed to.

## Install required system packages
#### Login into the server, e.g.
```bash
ssh -i ~/.ssh/cap-recruiter_server.pem ubuntu@cap-recruiter.cap-experiments.com
```

#### Update system
```bash
sudo apt update
sudo apt upgrade
```
#### Install packages
```bash
sudo apt install postgresql postgresql-server-dev-16 python3-pip python3-full pipx git redis apache2 supervisor
```

## Setup PostgreSQL
#### Login into PostreSQL console
```bash
sudo -u postgres -i
```
#### Create database user
```bash
createuser -P cap-recruiter --createdb
```
#### Create production database
```bash
createdb -O cap-recruiter cap-recruiter_production
```
#### Logout of PostreSQL console
```bash
exit
```
#### Reload PostgreSQL
```bash
sudo service postgresql reload
```

## Setup SSH public key access
#### Generate a keypair
```bash
ssh-keygen -t ed25519
```

#### Create system user 'cap'
```bash
sudo adduser cap
```
#### Add user 'cap' to sudoers `/etc/sudoers`
```bash
cap     ALL=(ALL:ALL) ALL
```

#### Change to user 'cap'
```bash
sudo su - cap
```
#### Exit the server login
```bash
exit  # 1
exit  # 2
```

#### Login into the server as user 'cap', e.g. ${\textsf{\color{orange}LOCAL}}$
```bash
ssh -i ~/.ssh/cap-recruiter_server.pem cap@cap-recruiter.cap-experiments.com
```
#### Generate a keypair
```bash
ssh-keygen -t ed25519
```
```bash
touch .ssh/authorized_keys
```
#### Add your SSH public key to cap user's `.ssh/authorized_keys` file
```
cat ~/.ssh/id_ed25519.pub  # On you local computer
```

#### Add deployment keys in GitLab by importing keypair of user 'cap' into the project's deploy keys in GitLab.
```
cat ~/.ssh/id_ed25519.pub
```

## Deployment

#### Deploy production branch ${\textsf{\color{orange}LOCAL}}$
```bash
cd deploy
fab deploy:branch=production
```

Deployment will fail at some point because we are missing an `.env` file

#### Create `.env` file in deployment directory created above
```bash
# GENERAL
DJANGO_ENV=production
ALLOWED_HOSTS=0.0.0.0,127.0.0.1,localhost

# PostgreSQL database
DB_NAME="cap-recruiter_production"
DB_USER="cap-recruiter"
DB_PASSWORD="cap-recruiter"
DB_HOST="localhost"

# Email server
EMAIL_HOST=localhost
EMAIL_PORT=1025

# production/staging settings
BASE_DIR=/var/www
DOMAIN=cap-recruiter.cap-experiments.com

# Recruiter class
RECRUITER=CapRecruiter

# WEBSOCKETS
WS_PROTOCOL=wss
WS_HOST=cap-recruiter.cap-experiments.com
```

#### Deploy production branch AGAIN ${\textsf{\color{orange}LOCAL}}$
```bash
fab deploy:branch=production
```

Deployment will fail at some point because of wrong log file permissions

#### Add symbolic links for Apache
```bash
sudo ln -s /home/cap/apps/cap-recruiter_production /var/www/production-cap-recruiter.cap-experiments.com
```

#### Assure correct permissions
```bash
sudo chown cap:cap /var/www/production-cap-recruiter.cap-experiments.com/cap-recruiter/log/celery.beat.log
sudo chown cap:cap /var/www/production-cap-recruiter.cap-experiments.com/cap-recruiter/log/celery.worker.log
```

#### Deploy production branch AGAIN ${\textsf{\color{orange}LOCAL}}$
```bash
fab deploy:branch=production
cd..
```

### Setup Django database
```bash
cd ~/apps/cap-recruiter_production/cap-recruiter/
pipenv shell
python3 manage.py createsuperuser --username admin --email admin@localhost
```

## Setup supervisor
#### Add supervisor group
```bash
sudo groupadd supervisor
sudo usermod -a -G supervisor cap
```

#### Modify supervisor config to restart gunicorn as user cap
#### Add/Update following lines in `/etc/supervisor/supervisord.conf`
```bash
chmod=0770
chown=root:supervisor
```

### Create supervisor config file `/etc/supervisor/conf.d/cap-recruiter.conf`
```bash
###########################
###  daphne production  ###
###########################
[program:cap-recruiter_production_daphne]
command=/home/cap/.local/share/virtualenvs/cap-recruiter-dkY5DymY/bin/daphne --bind 127.0.0.1 --port 8000 cap_recruiter.asgi:application
directory=/var/www/production-cap-recruiter.cap-experiments.com/cap-recruiter
user=cap
autostart=true
redirect_stderr=true

###########################
###  celery production  ###
###########################
[program:cap-recruiter_production_celery_beat]
command=/home/cap/.local/share/virtualenvs/cap-recruiter-dkY5DymY/bin/celery -A experiments beat -l info -f /var/www/production-cap-recruiter.cap-experiments.com/cap-recruiter/log/celery.beat.log
directory=/var/www/production-cap-recruiter.cap-experiments.com/cap-recruiter
stdout_logfile=/var/www/production-cap-recruiter.cap-experiments.com/cap-recruiter/log/celery.beat.log
stderr_logfile=/var/www/production-cap-recruiter.cap-experiments.com/cap-recruiter/log/celery.beat.log
user=cap
autostart=true
# autorestart=true
redirect_stderr=true
startsecs=10
stopasgroup=true

[program:cap-recruiter_production_celery]
command=/home/cap/.local/share/virtualenvs/cap-recruiter-dkY5DymY/bin/celery -A experiments worker -l info -f /var/www/production-cap-recruiter.cap-experiments.com/cap-recruiter/log/celery.worker.log
directory=/var/www/production-cap-recruiter.cap-experiments.com/cap-recruiter
stdout_logfile=/var/www/production-cap-recruiter.cap-experiments.com/cap-recruiter/log/celery.worker.log
stderr_logfile=/var/www/production-cap-recruiter.cap-experiments.com/cap-recruiter/log/celery.worker.log
user=cap
autostart=true
# autorestart=true
redirect_stderr=true
startsecs=10
stopasgroup=true
```

#### Restart supervisor
```bash
sudo service supervisor stop
sudo service supervisor start
sudo supervisorctl restart all
```

```bash
fab deploy:branch=production
cd..
```

### Verify all processes are running
```bash
sudo supervisorctl status
```

#### Assure correct permissions
```bash
chmod -R o+x /home/cap/apps/cap-recruiter_production
```

#### Add `CSRF_TRUSTED_ORIGINS` info to `cap_recruiter/settings/production.py`
```bash
CSRF_TRUSTED_ORIGINS = ["https://cap-recruiter.cap-experiments.com"]
```

# Add Apache config files
### `/etc/apache2/sites-available/cap-recruiter.cap-experiments.com.conf`
```bash
<VirtualHost _default_:80>
        Redirect 301 / https://cap-recruiter.cap-experiments.com
        ServerName cap-recruiter.cap-experiments.com
        ServerAdmin frank.hoeger@cap-experiments.com

        RewriteEngine   on
        RewriteCond     %{SERVER_NAME} =cap-recruiter.cap-experiments.com
        RewriteRule     ^ https://%{SERVER_NAME}%{REQUEST_URI} [END,NE,R=permanent]
</VirtualHost>
# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
```

### `/etc/apache2/sites-available/cap-recruiter.cap-experiments.com-ssl.conf`
```bash
<IfModule mod_ssl.c>
        <VirtualHost _default_:443>
                ServerName cap-recruiter.cap-experiments.com
                ServerAdmin frank.hoeger@cap-experiments.com

                DocumentRoot /var/www/production-cap-recruiter.cap-experiments.com

                ErrorLog ${APACHE_LOG_DIR}/production-cap-recruiter.cap-experiments.com_error.log
                CustomLog ${APACHE_LOG_DIR}/production-cap-recruiter.cap-experiments.com_access.log combined
                ProxyPass /static/ !
                ProxyPass / http://localhost:8000/
                RewriteEngine on
                RewriteCond %{HTTP:Upgrade} websocket [NC]
                RewriteCond %{HTTP:Connection} upgrade [NC]
                RewriteRule ^/?(.*) "ws://localhost:8000/$1" [P,L]

                Alias /static /var/www/production-cap-recruiter.cap-experiments.com/cap-recruiter/static

                SSLEngine on
                SSLCertificateFile      /etc/ssl/certs/cap-recruiter_cap_experiments_com.pem
                SSLCertificateKeyFile   /etc/ssl/private/cap-recruiter_cap_experiments_com.key

                <FilesMatch "\.(cgi|shtml|phtml|php)$">
                                SSLOptions +StdEnvVars
                </FilesMatch>
                <Directory /usr/lib/cgi-bin>
                                SSLOptions +StdEnvVars
                </Directory>

        </VirtualHost>
</IfModule>
# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
```

## Apache

### Add SSL certificate
#### Install certbot
```bash
sudo apt install certbot python3-certbot-apache
```

#### Create certificate
```bash
sudo certbot --apache -d cap-recruiter-2.cap-experiments.com -d cap-recruiter-2.cap-experiments.com
```

#### Add Apache modules
```bash
sudo a2enmod proxy_http
sudo a2enmod proxy_wstunnel
sudo a2enmod rewrite
sudo a2enmod ssl
sudo systemctl restart apache2
```

### Test app by logging in as admin user (admin@localhost)

https://cap-recruiter-2.cap-experiments.com


### Create `api-user` user and authentication token
```bash
python manage.py createsuperuser --username api-admin --email api-admin@production-cap-recruiter.cap-experiments.com
python manage.py drf_create_token api-admin
```

## Setup a PsyNet experiment ${\textsf{\color{orange}LOCAL}}$
Add the displayed token to the experiment's environment variables, e.g. on Heroku:
`CAP_RECRUITER_AUTH_TOKEN` `Token da128d326d10a5fc336af3efcddb7ae133ed5d83`

Set recruiter class name in in the experiment's config.txt, e.g.:
```
[Recruitment strategy]
recruiter = CapRecruiter
```
