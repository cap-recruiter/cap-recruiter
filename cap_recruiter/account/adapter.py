from allauth.account.adapter import DefaultAccountAdapter
from decouple import config
from django.contrib.sites.shortcuts import get_current_site


class CustomAccountAdapter(DefaultAccountAdapter):
    def get_email_confirmation_url(self, request, emailconfirmation):
        scheme_and_current_site = (
            f"https://{get_current_site(request)}"
            if config("DJANGO_ENV") in ["staging", "production"]
            else "http://127.0.0.1:8000"
        )
        return (
            f"{scheme_and_current_site}/accounts/confirm-email/{emailconfirmation.key}"
        )
