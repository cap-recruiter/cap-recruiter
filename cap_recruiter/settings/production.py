from decouple import config

from .base import *  # noqa: F403, F401

DEBUG = False

ADMINS = [("Frank Höger", "frank.hoeger@ae.mpg.de")]

STATIC_ROOT = f'{config("BASE_DIR")}/{config("DJANGO_ENV")}-{config("DOMAIN")}/cap-recruiter/static'
STATICFILES_STORAGE = "django.contrib.staticfiles.storage.ManifestStaticFilesStorage"

EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
EMAIL_HOST = "mailer.gwdg.de"
EMAIL_PORT = 25
DEFAULT_FROM_EMAIL = "cap-information@ae.mpg.de"
SERVER_EMAIL = "cap-information@ae.mpg.de"

CSRF_TRUSTED_ORIGINS = ["https://cap-recruiter.ae.mpg.de"]

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "root": {"level": "INFO", "handlers": ["file"]},
    "handlers": {
        "file": {
            "level": "INFO",
            "class": "logging.FileHandler",
            "filename": "log/django.log",
            "formatter": "app",
        },
        "mail_admins": {
            "level": "ERROR",
            "class": "django.utils.log.AdminEmailHandler",
            "include_html": True,
        },
    },
    "loggers": {
        "django": {
            "handlers": ["file", "mail_admins"],
            "level": "INFO",
            "propagate": True,
        },
    },
    "formatters": {
        "app": {
            "format": (
                "%(asctime)s [%(levelname)-8s] " "(%(module)s.%(funcName)s) %(message)s"
            ),
            "datefmt": "%Y-%m-%d %H:%M:%S",
        },
    },
}
