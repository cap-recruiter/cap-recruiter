from .base import *  # noqa: F403, F401

DEBUG = True

CORS_ALLOW_ALL_ORIGINS = True

STATIC_URL = "http://localhost:8080/"
