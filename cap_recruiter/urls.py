from django.contrib import admin
from django.http import HttpResponseRedirect
from django.urls import include, path, re_path

from accounts.views import ConsentPageView

urlpatterns = [
    re_path(r"^$", lambda r: HttpResponseRedirect("accounts/login")),
    path("admin/", admin.site.urls),
    path("hijack/", include("hijack.urls")),
    path(
        "consent",
        ConsentPageView.as_view(),
        name="consent",
    ),
    path("accounts/", include("allauth.urls")),
    re_path(r"^", include("experiments.urls")),
    re_path(r"^experiments/", include("experiments.urls")),
]
