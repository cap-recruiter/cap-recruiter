{% extends "account/email/base_message.txt" %}
{% load account %}
{% load i18n %}

{% block content %}
{% autoescape off %}
{% blocktrans %}
Dear participant,

Thanks for registering in CAP-Recruiter. We are excited to do science together!

Before starting, you need to verify that this is your email address by clicking here: {{ activate_url }}

If you have any questions, please send an email to cap-information@ae.mpg.de.
{% endblocktrans %}
{% endautoescape %}
{% endblock %}
