"""
ASGI config for cap_recruiter project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/asgi/
"""

import os

from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
from decouple import config
from django.core.asgi import get_asgi_application

os.environ.setdefault(
    "DJANGO_SETTINGS_MODULE", f"cap_recruiter.settings.{config('DJANGO_ENV')}"
)
application = get_asgi_application()

from experiments.routing import ws_urlpatterns  # noqa: E402

application = ProtocolTypeRouter(
    {
        "http": application,
        "websocket": AuthMiddlewareStack(URLRouter(ws_urlpatterns)),
    }
)
