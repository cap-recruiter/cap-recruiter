const Webpack = require('webpack');
const Path = require('path');

module.exports = {
  entry: {
    app: Path.resolve(__dirname, '../src/scripts/index.js'),
    vendor: Path.resolve(__dirname, '../src/scripts/vendor.js'),
  },
  plugins: [
    new Webpack.ProvidePlugin({
      'window.jQuery': 'jquery',
      'jQuery': 'jquery',
      '$': 'jquery'
    }),
  ],
}
