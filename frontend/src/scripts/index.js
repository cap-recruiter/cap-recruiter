import '../styles/base.scss';

import './base.js';
import './sort.js';


document.addEventListener('DOMContentLoaded', () => {
  document.querySelectorAll(
    '.frontend-module'
  ).forEach((element) => {
    if ('name' in element.dataset) {
      import(
        `./${element.dataset.name}`
      ).then((module) => {
        module.default();
      });
    }
  });
});

