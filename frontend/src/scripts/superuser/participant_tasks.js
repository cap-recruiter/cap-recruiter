/*global wsProtocol, wsHost, customUserId, participantId, participantEmail*/
/*eslint no-undef: "error"*/

import { setCompletedAt, setFailedAt, setStartedAt, setResetAt } from './tasks.js';

export default function connect() {
  var socket = new WebSocket(wsProtocol +'://' + wsHost + '/wss/admin_tasks/');

  socket.onopen = function() {
    socket.send(JSON.stringify({
      'custom_user_id': customUserId,
      'worker_id': participantId
    }));
  };

  socket.onmessage = function(event) {
    let data = JSON.parse(event.data);
    $.each(data.tasks, function(taskId, task) {
      if ($('#' + taskId).length == 0) {
        location.reload();
      }
      $('#started_at-' + taskId).text(task.started_at);
      if (task.state == 'completed') {
        setCompletedAt(task);
      } else if (task.state == 'not_completed') {
        setFailedAt(task);
      } else if (task.state == 'started') {
        setStartedAt(task);
      } else if (task.state == 'reset') {
        setResetAt(task);
      }
      if (['paid', 'received'].includes(task.payment.state)) {
        if (task.state == "completed") {
          setCompletedAt(task);
        } else if (task.state == "failed") {
          setFailedAt(task);
        }
      }
      $('#duration-' + taskId).text(task.duration);
      $('#base_payment-' + taskId).text(task.base_payment);
      $('#bonus-' + taskId).text(task.bonus);
      let total_payment = task.state == 'reset' ? '<span style="color: #ffc107;">' + task.payment.amount + '</span>' : task.payment.amount;
      $('#total_payment-' + taskId).html(total_payment);
      $('#payment-done-at-' + taskId).text(task.payment.paid_at);
      $('#payment-received-at-' + taskId).text(task.payment.received_at);
      if (['completed', 'not_completed'].includes(task.state) && task.payment.state == "unpaid" && task.total_payment_as_number > 0) {
        $('#payment_done-' + taskId).show();
      } else {
        $('#payment_done-' + taskId).hide();
      }
      if (task.state == 'not_completed') {
        $('#delete-payment-' + taskId).show();
      }

    });

    if (data['has_outstanding_payments']) {
      $('#outstanding_payments_sum').text('$' + data['outstanding_payments_in_dollars']);
      $('#outstanding_payments').show();
      if (data['has_finished_non_test_task']) {
        $('#outstanding_payments_sum').addClass('outstanding_payment');
        $('#outstanding_payments_sum').removeClass('test_task_payment');
      }
      $('#pay-all-tasks').show();
    } else {
      $('#pay-all-tasks').hide();
    }
    $('#total-payments').text(data['total_payments']);

    socket.onclose = function(e) {
      console.log('Socket is closed. Attempting to reconnect in one second.', e.reason);
      setTimeout(function() {
        connect();
      }, 1000);
    };

    socket.onerror = function(err) {
      console.error('Socket error: ', err.message, 'Closing socket.');
      socket.close();
    };
  };

  $('.pay-task').unbind('click').click(function() {
    window.location = '/tasks/pay/' + $(this).data('task-id') + '?redirect_to=participant';
  });

  $('#pay-all-tasks').on('click', function() {
    window.location = '/experiments/experiments/participants/' + participantId + '/tasks/pay_sum';
  });

  // Copy to clipboard
  $('#copy-participant-email').click(function() {
    let element = document.createElement('textarea');
    element.value = participantEmail;
    document.body.appendChild(element);
    element.select();
    document.execCommand('copy');
    document.body.removeChild(element);
  });

  $('.btn-delete').on('click', function() {
    let taskId = $(this).data("task-id");
    $("#delete-task").data("task-id", taskId);
  });

  $('#delete-task').on('click', function() {
    let taskId = $(this).data("task-id");
    $.ajax({
      type: 'GET',
      url: "/tasks/" + taskId + "/delete",
      success: function () {
        console.log("Task with id '" + taskId + "' deleted.");
        window.location.reload();
      },
      error: function (response) {
        console.log("There was an error deleting the Task:");
        console.log(response);
      }
    });
  });
}

connect();
