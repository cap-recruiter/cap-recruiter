export default function _default() {
  $('#csv_import_file_button').css('cursor', 'pointer');
  $('#csv_import_file_button').on('click', function(){
    document.importForm.submit();
  });

  document.querySelector('.custom-file-input').addEventListener('change',function(){
    var fileName = document.getElementById("csv_import_file").files[0].name;
    $(".custom-file-label").html(fileName);
  });
}
