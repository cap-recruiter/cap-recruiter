from decouple import config
from django import template
from django.contrib.sites.shortcuts import get_current_site

register = template.Library()


@register.simple_tag
def bugfix_url(url, request):
    if config("DJANGO_ENV") == "development":
        return url

    scheme = "https" if config("DJANGO_ENV") in ["staging", "production"] else "http"
    port = 8000 if config("DJANGO_ENV") == "production" else 8001
    scheme_and_domain = f"{scheme}://{get_current_site(request)}"

    return url.replace(f"http://localhost:{port}", scheme_and_domain)


@register.simple_tag
def invitation_link(group_manager, request):
    return group_manager.invitation_link(request)
