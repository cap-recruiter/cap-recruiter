from datetime import date, datetime
from random import shuffle

import pycountry
from auditlog.registry import auditlog
from django.contrib.auth.models import AbstractUser
from django.contrib.sites.shortcuts import get_current_site
from django.db import models
from django.db.models import Q, Sum
from django.shortcuts import get_object_or_404
from django.urls import reverse

from experiments.models import Task


class CustomUser(AbstractUser):
    worker_id = models.CharField(max_length=10, blank=False, default="")
    is_group_manager = models.BooleanField(default=False)
    nonresponsive = models.BooleanField(default=False)
    test_task_verified = models.BooleanField(default=False)
    email = models.EmailField(unique=True)
    deleted_at = models.DateTimeField(db_index=True, null=True)

    # foreign keys
    group_manager = models.ForeignKey(
        "CustomUser",
        null=True,
        default=None,
        on_delete=models.CASCADE,
        related_name="customusers_group_manager_id",
    )

    def __str__(self):
        return f"CustomUser object ({self.id})"

    def age(self):
        birthday = datetime.strptime(self.demographic_info.date_of_birth, "%Y-%m-%d")
        today = date.today()
        return (
            today.year
            - birthday.year
            - ((today.month, today.day) < (birthday.month, birthday.day))
        )

    def date_of_birth(self):
        return self.demographic_info.date_of_birth

    def gender(self):
        return self.demographic_info.gender

    def years_of_experience(self):
        return self.demographic_info.years_of_experience

    def country_name(self):
        if not self.demographic_info.country_code:
            return ""
        return pycountry.countries.get(alpha_2=self.demographic_info.country_code).name

    def country_code(self):
        return self.demographic_info.country_code

    def group_manager_code(self):
        if not self.group_manager:
            return ""
        return self.group_manager.groupmanager.code

    def invitation_link(self, request):
        from decouple import config

        scheme_and_domain = ""
        if config("DJANGO_ENV") == "development":
            scheme_and_domain = "http://localhost:8000"
        else:
            scheme_and_domain = f"https://{get_current_site(request)}"

        return f"{scheme_and_domain}/accounts/signup/?group={self.group_manager_code()}"

    def display_notice(self):
        return self.group_manager.groupmanager.display_notice

    def notice(self):
        return self.group_manager.groupmanager.notice

    def group_manager_consent(self):
        return self.group_manager.groupmanager.consent

    def consent_name(self):
        return self.group_manager.groupmanager.consent_name()

    def consent(self):
        return self.group_manager.groupmanager.consent

    def enable_experiment_start_buttons(self):
        return self.group_manager.groupmanager.enable_experiment_start_buttons

    def group_manager_name(self):
        return self.groupmanager.name

    def group_manager_full_name(self):
        return f"{self.group_manager.first_name} {self.group_manager.last_name}"

    def group_manager_email(self):
        return self.group_manager.email

    def initial_test_experiment(self):
        return self.group_manager.groupmanager.initial_test_experiment

    def available_experiments(self, deployed_experiment_ids):
        if not self.group_manager:
            return None

        active_experiments = (
            GroupManager.objects.filter(code=self.group_manager_code())
            .first()
            .experiments.filter(expire_date__gte=datetime.today())
            .exclude(deleted_at__isnull=False)
            .order_by("-created_at")
        )
        active_and_deployed_experiments = [
            exp for exp in active_experiments if exp.id in deployed_experiment_ids
        ]
        shuffle(active_and_deployed_experiments)

        batched_experiments = {}
        for experiment in active_and_deployed_experiments:
            batches = list(range(1, experiment.batches + 1))
            completed_experiment_batches = list(
                Task.objects.filter(experiment_id=experiment.id)
                .filter(worker_id=self.id)
                .filter(reset_at__isnull=True)
                .values_list("batch", flat=True)
            )
            for batch in completed_experiment_batches:
                try:
                    batches.remove(batch)
                except ValueError:
                    pass
            if batches:
                shuffle(batches)
                batched_experiments[experiment.id] = {
                    "batches": [batches[0]],
                    "experiment": experiment,
                }

        return batched_experiments

    def experiments(self):
        if not self.group_manager:
            return None
        return get_object_or_404(
            GroupManager, code=self.group_manager_code()
        ).experiments

    def full_name(self):
        if any([self.first_name, self.last_name]):
            return f"{self.first_name} {self.last_name}"
        else:
            return self.username

    def participants(self):
        if not self.is_group_manager:
            return None
        return CustomUser.objects.filter(group_manager_id=self.id).exclude(id=self.id)

    def participants_not_deleted(self):
        if not self.is_group_manager:
            return None
        return (
            CustomUser.objects.filter(group_manager_id=self.id)
            .exclude(id=self.id)
            .exclude(deleted_at__isnull=False)
        )

    def participants_not_deleted_count(self):
        return self.participants_not_deleted().count()

    def worker_tasks(self):
        return Task.objects.filter(worker_id=self.id)

    def worker_tasks_count(self):
        return self.worker_tasks().count()

    def worker_test_task(self):
        task = self.worker_tasks().order_by("id").first()
        if task is not None and task.is_test_task():
            return task
        return None

    def worker_test_task_experiment(self):
        return (
            GroupManager.objects.filter(code=self.group_manager_code())
            .first()
            .experiments.filter(use_for_test_tasks=True)
            .filter(expire_date__gte=datetime.today())
            .exclude(deleted_at__isnull=False)
            .first()
        )

    def group_manager_tasks(self):
        return Task.objects.filter(group_manager_id=self.group_manager.groupmanager.id)

    def type(self):
        if self.is_group_manager:
            return "group_manager"
        elif self.is_superuser:
            return "superuser"
        else:
            return "worker"

    def get_absolute_url(self):
        return reverse("experiments-group_managers")

    def total_payments(self):
        tasks = self.worker_tasks()

        if not tasks:
            return 0

        base_payment_sum = tasks.aggregate(Sum("base_payment")).get("base_payment__sum")
        if base_payment_sum is None:
            return 0

        bonus_sum = tasks.aggregate(Sum("bonus")).get("bonus__sum")

        return bonus_sum + base_payment_sum

    def outstanding_payments(self):
        completed_tasks = self.worker_tasks().filter(
            Q(state="completed") | Q(state="not_completed") | Q(state="reset")
        )
        completed_task_ids = [
            completed_task.id
            for completed_task in completed_tasks
            if completed_task.payment.state == "unpaid"
        ]
        return Task.objects.filter(pk__in=completed_task_ids)

    def has_outstanding_payments(self):
        return len(self.outstanding_payments()) > 0

    def outstanding_payments_sum(self):
        outstanding_payments = self.outstanding_payments()
        base_payment_sum = outstanding_payments.aggregate(Sum("base_payment")).get(
            "base_payment__sum"
        )
        bonus_sum = outstanding_payments.aggregate(Sum("bonus")).get("bonus__sum")
        if base_payment_sum is None and bonus_sum is None:
            return 0
        return round(base_payment_sum + bonus_sum, 2)

    def primary_emailaddress(self):
        return self.emailaddress_set.filter(primary=True).first()

    def is_verified(self):
        if not self.primary_emailaddress():
            return False
        return self.primary_emailaddress().verified

    def is_deleted(self):
        if self.deleted_at is None:
            return False
        return True

    def has_finished_non_test_task(self):
        last_task = self.worker_tasks().filter(state="completed").order_by("id").last()
        if last_task is None:
            return False
        if not last_task.is_test_task():
            return True
        return False

    def has_failed_tasks(self):
        if self.worker_tasks().filter(state="not_completed").exists():
            return True
        return False


class CustomUserDemographicInfo(models.Model):
    gender = models.CharField(max_length=100, blank=False, default="")
    country_code = models.CharField(max_length=2, blank=False, default="")
    date_of_birth = models.CharField(max_length=50, blank=False, default="1970-01-01")
    years_of_experience = models.FloatField(blank=False, default=0)

    custom_user = models.OneToOneField(
        CustomUser,
        on_delete=models.CASCADE,
        primary_key=True,
        related_name="demographic_info",
    )


class GroupManager(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=100, unique=True)
    display_notice = models.BooleanField(null=False, default=False, blank=True)
    notice = models.TextField(null=False, blank=True, default="")
    enable_experiment_start_buttons = models.BooleanField(default=True, blank=True)
    consent = models.CharField(max_length=100, null=False, blank=False, default="mpiae")

    experiments = models.ManyToManyField(
        "experiments.Experiment", related_name="group_managers"
    )

    # foreign keys
    initial_test_experiment = models.ForeignKey(
        "experiments.Experiment",
        null=True,
        on_delete=models.SET_NULL,
        related_name="initial_test_experiment",
    )

    def full_name(self):
        first_name = self.user.first_name
        last_name = self.user.last_name
        if any([first_name, last_name]):
            return f"{first_name} {last_name}"
        else:
            return self.username

    def consent_name(self):
        from accounts.forms import consent_choices

        return dict(consent_choices())[self.consent]


class Message(models.Model):
    subject = models.CharField(max_length=200, blank=False)
    text = models.CharField(blank=False)
    recipient_ids = models.JSONField()

    # foreign keys
    sender = models.ForeignKey(
        "CustomUser",
        null=True,
        default=None,
        on_delete=models.SET_NULL,
        related_name="messages",
    )

    # timestamps
    send_at = models.DateTimeField(null=False)


auditlog.register(
    CustomUser,
    exclude_fields=[
        "last_login",
        "password",
        "first_name",
        "last_name",
        "email",
        "username",
        "worker_id",
    ],
)
auditlog.register(
    CustomUserDemographicInfo,
    exclude_fields=["gender", "country_code", "date_of_birth", "years_of_experience"],
)
auditlog.register(GroupManager)
