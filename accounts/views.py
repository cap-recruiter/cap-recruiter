import json
import logging
from datetime import datetime

from allauth.account.models import EmailAddress
from decouple import config
from django.contrib import messages
from django.contrib.auth import logout
from django.db import transaction
from django.http import HttpResponse
from django.views.generic import TemplateView
from rest_framework.views import APIView

from accounts.models import CustomUser, GroupManager
from experiments.models import Experiment
from experiments.views import ContextMixin, menu_items

logger = logging.getLogger(__name__)


class DemographicInfoDeleteView(TemplateView):
    def get(self, request, pk=None):
        try:
            with transaction.atomic():
                user = request.user
                if user.is_superuser:
                    user = CustomUser.objects.get(id=pk)
                original_full_name = user.full_name()
                user.demographic_info.delete()
                EmailAddress.objects.get(email=user.email).delete()
                user.is_active = False
                user.username = f"Participant ({pk})"
                user.first_name = ""
                user.last_name = ""
                user.worker_id = ""
                user.last_login = None
                user.email = f"Deleted ({str(datetime.now())})"
                user.deleted_at = datetime.now()
                user.save()

                if not request.user.is_superuser:
                    logout(request)
                    messages.success(
                        request, "Your account data has been permanently erased."
                    )
                else:
                    messages.success(
                        request,
                        f"Account data for '{original_full_name}' has been erased.",
                    )
            return HttpResponse(status=200)
        except Exception as e:
            error_msg = f"Error deleting user: {e}"
            logger.error(error_msg)
            return HttpResponse(error_msg, status=500)


class ConsentPageView(ContextMixin, TemplateView):
    template_name = "account/consent.html"

    def get_context_data(self, *args, **kwargs):
        context = super(ConsentPageView, self).get_context_data(
            self.request, *args, **kwargs
        )
        code = self.request.GET.get("code")
        if code is not None:
            context["consent"] = GroupManager.objects.filter(code=code).first().consent
        else:
            consent = self.request.GET.get("consent")
            context["consent"] = (
                GroupManager.objects.filter(consent=consent).first().consent
            )
        return context


class ConsentInfoView(ContextMixin, TemplateView):
    template_name = "consent_info.html"

    def get_context_data(self, *args, **kwargs):
        context = super(ConsentInfoView, self).get_context_data(
            self.request, *args, **kwargs
        )
        deployed_experiment_ids = Experiment.deployed_experiments()

        if self.request.user.is_authenticated:
            if self.request.user.worker_id:
                if self.request.user.test_task_verified:
                    context["batched_experiments"] = (
                        self.request.user.available_experiments(deployed_experiment_ids)
                    )
                else:
                    test_tasks_experiment = self.request.user.initial_test_experiment()
                    if (
                        test_tasks_experiment
                        and test_tasks_experiment.id in deployed_experiment_ids
                    ):
                        context["test_tasks_experiment"] = test_tasks_experiment
                context["deployed_experiments"] = deployed_experiment_ids
                context["tasks"] = self.request.user.worker_tasks().order_by("-id")
                context["group_manager_code"] = self.request.user.group_manager_code()
                context["worker_id"] = self.request.user.worker_id
            context["django_env"] = config("DJANGO_ENV")
            context["menu_items"] = menu_items(self.request.user)
            context["recruiter"] = config("RECRUITER")
        return context

    def menu_items(self):
        return menu_items(self.request.user)


class GroupManagerCheckView(ContextMixin, APIView):
    """
    This class checks for a valid group_manager code.
    """

    def get(self, request):
        group_manager = GroupManager.objects.filter(
            code=request.GET.get("groupManagerCode")
        ).first()

        response_data = {}
        response_data["valid"] = True if group_manager is not None else False

        return HttpResponse(json.dumps(response_data), content_type="application/json")
