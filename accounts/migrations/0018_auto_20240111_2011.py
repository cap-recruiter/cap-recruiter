# Generated by Django 4.2.9 on 2024-01-11 19:11

from django.db import migrations


def combine_names(apps, schema_editor):
    GroupManager = apps.get_model("accounts", "GroupManager")
    for group_manager in GroupManager.objects.all():
        group_manager.name = (
            f"{group_manager.user.first_name} {group_manager.user.last_name}"
        )
        group_manager.save()


class Migration(migrations.Migration):
    dependencies = [
        ("accounts", "0017_groupmanager_name"),
    ]

    operations = [
        migrations.RunPython(combine_names),
    ]
