# Generated by Django 4.1.2 on 2022-10-27 02:49

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("accounts", "0013_auto_20210819_1110"),
    ]

    operations = [
        migrations.RenameField(
            model_name="customuser",
            old_name="test_hit_verified",
            new_name="test_task_verified",
        ),
    ]
