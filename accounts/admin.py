from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .forms import CustomUserChangeForm, CustomUserCreationForm
from .models import CustomUser


class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = CustomUser
    list_display = [
        "username",
        "first_name",
        "last_name",
        "email",
        "is_superuser",
        "is_group_manager",
        "group_manager_id",
    ]

    add_fieldsets = UserAdmin.add_fieldsets + (
        (
            None,
            {
                "fields": [
                    "email",
                    "first_name",
                    "last_name",
                    "is_group_manager",
                    "group_manager",
                ]
            },
        ),
    )


admin.site.register(CustomUser, CustomUserAdmin)
