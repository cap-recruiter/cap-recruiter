from django.contrib.auth import get_user_model
from django.test import Client, TransactionTestCase
from django.urls import reverse

from .models import CustomUserDemographicInfo, GroupManager

CustomUser = get_user_model()


class LoginPageTests(TransactionTestCase):
    def setUp(self):
        self.group_manager_custom_user = CustomUser(
            username="group_manager_1",
            email="gm_1@localhost",
            first_name="Gerd",
            last_name="Gott",
            is_group_manager=True,
        )
        self.group_manager_custom_user.set_password("some_password_gm1")
        self.group_manager_custom_user.save()
        self.group_manager_custom_user.group_manager_id = (
            self.group_manager_custom_user.id
        )

        self.group_manager = GroupManager.objects.create(
            code="capcode123456",
            user_id=self.group_manager_custom_user.id,
            display_notice=True,
            notice="some notice",
            enable_experiment_start_buttons=True,
        )
        CustomUserDemographicInfo.objects.create(
            custom_user=self.group_manager_custom_user,
            country_code="DE",
        )

        self.worker_email = "worker_1@localhost"
        self.worker = CustomUser(
            username="worker_1",
            email=self.worker_email,
            first_name="Anna",
            last_name="Alberg",
            worker_id="AA123456",
            group_manager_id=self.group_manager_custom_user.id,
        )
        self.worker_password = "some_password_w1"
        self.worker.set_password(self.worker_password)
        self.worker.save()

    def test_users_exist(self):
        self.assertEqual(CustomUser.objects.all().count(), 2)

    def test_group_manager_exists(self):
        self.assertEqual(GroupManager.objects.all().count(), 1)

    def test_worker_password(self):
        self.assertTrue(self.worker.check_password(self.worker_password))

    def test_login_page_status_code(self):
        response = self.client.get("/accounts/login/")
        self.assertEqual(response.status_code, 200)

    def test_successful_login(self):
        self.client = Client()
        assert "sessionid" not in self.client.cookies

        login_data = {"login": self.worker_email, "password": self.worker_password}
        response = self.client.post(reverse("account_login"), login_data)
        assert "sessionid" in self.client.cookies

        self.assertRedirects(response, "/experiments/")
        self.assertEqual(response.status_code, 302)

    def test_unsuccessful_login(self):
        self.client = Client()
        assert "sessionid" not in self.client.cookies

        login_data = {"login": self.worker_email, "password": "WRONG_PASSWORD"}
        response = self.client.post(reverse("account_login"), login_data)
        assert "sessionid" not in self.client.cookies

        self.assertEqual(response.request.get("PATH_INFO"), "/accounts/login/")
        self.assertEqual(response.status_code, 200)
