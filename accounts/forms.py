import random
from datetime import date, datetime

from allauth.utils import generate_unique_username
from django import forms
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.core.exceptions import ValidationError
from django.shortcuts import get_object_or_404
from django.utils.safestring import mark_safe
from pycountry import countries

from .models import CustomUser, CustomUserDemographicInfo, GroupManager
from .utils import birthday_years, generate_worker_id


def consent_choices():
    return [("mpiae", "MPIAE"), ("princeton", "Princeton University")]


class CustomUserCreationForm(UserCreationForm):
    class Meta:
        model = CustomUser
        fields = (
            "username",
            "first_name",
            "last_name",
            "email",
        )


class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = CustomUser
        fields = (
            "username",
            "first_name",
            "last_name",
            "email",
        )


class CustomSignupForm(forms.Form):
    first_name = forms.CharField(max_length=30, label="First Name")
    last_name = forms.CharField(max_length=30, label="Last Name")

    gender_choices = (
        ("female", "Female"),
        ("male", "Male"),
        ("other", "Other"),
    )
    gender = forms.ChoiceField(
        label="Gender",
        choices=gender_choices,
        widget=forms.RadioSelect(attrs={"class": "form-check-inline"}),
    )

    country_choices = [("", "")] + sorted(
        [
            (country.alpha_2, country.name)
            for country in countries
            if hasattr(country, "alpha_2")
        ],
        key=lambda _country: _country[1],
    )
    country_code = forms.ChoiceField(label="Country", choices=country_choices)
    date_of_birth = forms.DateField(
        widget=forms.SelectDateWidget(years=birthday_years())
    )
    years_of_experience = forms.FloatField(
        label="How many years of regular practice on a musical instrument or singing have you had?",
        widget=forms.NumberInput(
            attrs={
                "id": "years_of_experience",
                "placeholder": "",
                "step": 0.5,
                "min": 0,
                "style": "text-align: center; width: 150px;",
            }
        ),
    )
    group_manager_code = forms.CharField(max_length=30, label="Group Manager Code")
    consent = forms.BooleanField(
        label=mark_safe(
            '<span style="cursor: pointer;">I have read and consent to <a id="consent_link" target="_blank" style="pointer-events: none;">Registering as a Study Participant</a></span>'
        ),
        required=True,
    )

    def signup(self, request, user):
        country_code = self.cleaned_data["country_code"]
        user.worker_id = generate_worker_id(user, country_code)
        user.group_manager_id = self.group_manager_id()
        if self.is_valid():
            user.save()
            CustomUserDemographicInfo.objects.create(
                custom_user=user,
                gender=self.cleaned_data["gender"],
                date_of_birth=self.cleaned_data["date_of_birth"],
                country_code=self.cleaned_data["country_code"],
                years_of_experience=self.cleaned_data["years_of_experience"],
            )

    field_order = [
        "first_name",
        "last_name",
        "gender",
        "date_of_birth",
        "country_code",
        "years_of_experience",
        "email",
        "password1",
        "password2",
    ]

    def clean_date_of_birth(self):
        date_of_birth = self.cleaned_data["date_of_birth"]
        birthday = datetime.strptime(str(date_of_birth), "%Y-%m-%d")
        today = date.today()
        age = (
            today.year
            - birthday.year
            - ((today.month, today.day) < (birthday.month, birthday.day))
        )
        if age < 18:
            raise ValidationError(
                ("You have to be older than 18 years to take part in experiments."),
                code="invalid_date_of_birth",
            )
        return date_of_birth

    def clean_group_manager_code(self):
        provided_group_manager_code = self.cleaned_data["group_manager_code"]
        group_manager_codes = [
            user.groupmanager.code
            for user in CustomUser.objects.filter(is_group_manager=True)
        ]
        if provided_group_manager_code not in group_manager_codes:
            raise ValidationError(
                (f"Invalid group manager code '{provided_group_manager_code}'."),
                code="invalid_group_manager_code",
            )
        return provided_group_manager_code

    def group_manager_id(self):
        return get_object_or_404(
            GroupManager, code=self.cleaned_data["group_manager_code"]
        ).user_id


class GroupManagerCreationForm(UserCreationForm):
    class Meta:
        model = CustomUser
        fields = [
            "name",
            "first_name",
            "last_name",
            "country_code",
            "email",
            "password1",
            "password2",
            "display_notice",
            "notice",
            "consent",
            "enable_experiment_start_buttons",
        ]

    def __init__(self, *args, **kwargs):
        super(GroupManagerCreationForm, self).__init__(*args, **kwargs)

    name = forms.CharField(max_length=100, label="Name")
    first_name = forms.CharField(max_length=30, label="First name (group manager)")
    last_name = forms.CharField(max_length=30, label="Last name (group manager)")
    email = forms.EmailField(max_length=50, label="Email")
    country_choices = [("", "")] + sorted(
        [
            (country.alpha_2, country.name)
            for country in countries
            if hasattr(country, "alpha_2")
        ],
        key=lambda _country: _country[1],
    )
    country_code = forms.ChoiceField(label="Country", choices=country_choices)
    display_notice = forms.BooleanField(label="Display notice", required=False)
    notice = forms.CharField(
        widget=forms.Textarea(attrs={"rows": 3}), label="Notice text", required=False
    )
    consent = forms.ChoiceField(
        label="Consent",
        choices=consent_choices(),
        widget=forms.Select(attrs={"style": "width: auto; cursor: pointer;"}),
    )
    enable_experiment_start_buttons = forms.BooleanField(
        label="Enable experiment start buttons", required=False
    )
    # initial_test_experiment = forms.ChoiceField(widget=forms.HiddenInput, required=False)

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        user.is_group_manager = True
        user.username = generate_unique_username([user.first_name, user.last_name])

        if commit:
            user.save()
            user.group_manager_id = user.id
            user.save()
            GroupManager.objects.create(
                name=self.cleaned_data["name"],
                code=f"capcode{random.randint(1, 999999):06d}",
                user_id=user.id,
                display_notice=self.cleaned_data["display_notice"],
                notice=self.cleaned_data["notice"],
                enable_experiment_start_buttons=self.cleaned_data[
                    "enable_experiment_start_buttons"
                ],
                # initial_test_experiment_id=self.cleaned_data[
                #     "initial_test_experiment"
                # ],
            )
            CustomUserDemographicInfo.objects.create(
                custom_user=user,
                country_code=self.cleaned_data["country_code"],
            )
        return user


class GroupManagerUpdateForm(UserChangeForm, forms.ModelForm):
    password = None

    class Meta:
        model = CustomUser
        fields = [
            "name",
            "first_name",
            "last_name",
            "country_code",
            "email",
            "display_notice",
            "notice",
            "consent",
            "enable_experiment_start_buttons",
        ]

    name = forms.CharField(max_length=100, label="Name")
    first_name = forms.CharField(max_length=30, label="First Name (group manager)")
    last_name = forms.CharField(max_length=30, label="Last Name (group manager)")
    email = forms.EmailField(max_length=50, label="Email")
    country_choices = [("", "")] + sorted(
        [
            (country.alpha_2, country.name)
            for country in countries
            if hasattr(country, "alpha_2")
        ],
        key=lambda _country: _country[1],
    )
    country_code = forms.ChoiceField(label="Country", choices=country_choices)
    display_notice = forms.BooleanField(label="Display notice", required=False)
    notice = forms.CharField(
        widget=forms.Textarea(attrs={"rows": 3}), label="Notice text", required=False
    )
    consent = forms.ChoiceField(
        label="Consent",
        choices=consent_choices(),
        widget=forms.Select(attrs={"style": "width: auto; cursor: pointer;"}),
    )
    enable_experiment_start_buttons = forms.BooleanField(
        label="Enable experiment start buttons", required=False
    )

    def __init__(self, *args, **kwargs):
        super(GroupManagerUpdateForm, self).__init__(*args, **kwargs)
        group_manager = get_object_or_404(GroupManager, user_id=kwargs["instance"].id)
        self.fields["name"].initial = group_manager.name
        self.fields["country_code"].initial = group_manager.user.country_code
        self.fields["display_notice"].initial = group_manager.display_notice
        self.fields["notice"].initial = group_manager.notice
        self.fields["consent"].initial = group_manager.user.consent
        self.fields["enable_experiment_start_buttons"].initial = (
            group_manager.enable_experiment_start_buttons
        )


class ParticipantUpdateForm(UserChangeForm):
    password = None

    class Meta:
        model = CustomUser
        fields = [
            "first_name",
            "last_name",
            "country_code",
            "gender",
            "date_of_birth",
            "years_of_experience",
            "nonresponsive",
        ]

    def __init__(self, *args, **kwargs):
        super(ParticipantUpdateForm, self).__init__(*args, **kwargs)
        custom_user = get_object_or_404(CustomUser, pk=kwargs["instance"].id)
        self.fields["country_code"].initial = custom_user.country_code
        self.fields["gender"].initial = custom_user.gender
        self.fields["date_of_birth"].initial = custom_user.date_of_birth
        self.fields["years_of_experience"].initial = (
            custom_user.demographic_info.years_of_experience
        )
        self.fields["nonresponsive"].initial = custom_user.nonresponsive

    first_name = forms.CharField(max_length=30, label="First Name")
    last_name = forms.CharField(max_length=30, label="Last Name")
    country_choices = [("", "")] + sorted(
        [
            (country.alpha_2, country.name)
            for country in countries
            if hasattr(country, "alpha_2")
        ],
        key=lambda _country: _country[1],
    )
    country_code = forms.ChoiceField(label="Country", choices=country_choices)
    gender_choices = (
        ("female", "Female"),
        ("male", "Male"),
        ("other", "Other"),
    )
    gender = forms.ChoiceField(
        label="Gender",
        choices=gender_choices,
        widget=forms.RadioSelect(attrs={"class": "form-check-inline"}),
    )
    date_of_birth = forms.DateField(
        widget=forms.SelectDateWidget(years=birthday_years())
    )
    years_of_experience = forms.FloatField(
        label="Years of musical experience",
        widget=forms.NumberInput(
            attrs={
                "id": "years_of_",
                "placeholder": "",
                "step": 0.5,
                "min": 0,
                "style": "text-align: center; width: 150px;",
            }
        ),
    )
    nonresponsive = forms.BooleanField(required=False)
