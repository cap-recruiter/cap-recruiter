import datetime
import random


def generate_worker_id(user, country_code):
    return (
        country_code
        + user.first_name[0].capitalize()
        + user.last_name[0].capitalize()
        + f"{random.randint(1, 999999):06d}"
    )


def birthday_years():
    this_year = datetime.date.today().year
    minimum_year = this_year - 17
    return list(reversed(range(this_year - 100, minimum_year)))
