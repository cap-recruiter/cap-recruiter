help:
	@echo "Please use \`make <target>' where <target> is one of"
	@echo "  clean        to delete files having pattern (*.pyc | *.pyo | *~)"
	@echo "  test         to run the complete set of tests"
	@echo "  test-report  to run the complete set of tests and generate test and coverage reports"

clean:
	find . -name '*.pyc' -exec rm --force {} \;
	find . -name '*.pyo' -exec rm --force {} \;
	find . -name '*~' -exec rm --force {} \;

test:
	make clean
	pytest -ra -q -s

test-report:
	make clean
	pytest -ra -q -s --html=tests/html/report.html --cov=cap_recruiter --cov=experiments --cov=accounts --cov-report html:tests/coverage
