# CAP-Recruiter – Developer installation
The following installation instructions apply to Ubuntu 22.04 LTS (Jammy Jellyfish) only. They address setting up the development environment on a local computer.

## Basic setup
### Install system packages
```bash
sudo apt install postgresql postgresql-server-dev-14 python3-pip git npm redis
```

### Install pipenv
```bash
pip install --user pipenv
echo -e '\nexport PATH="$PATH:~/.local/bin"' >> ~/.profile
```

IMPORTANT: Log out and log in again to make use of the changes in your `~/.profile` file


### Install cap-recruiter app

NOTICE: You need to have access rights to the GitLab respository.

```bash
mkdir ~/projects
cd ~/projects
git clone git@gitlab.com:computational-audition-lab/cap-recruiter.git
cd cap-recruiter
pipenv install --dev
pipenv shell
pre-commit install
```

### Install system packages
```console
cd frontend
npm install
```

### Create environment file
Go to the projects home directory
```bash
cd ~/projects/cap-recruiter
```
and create an `.env` file with following contents:
```
# General
ALLOWED_HOSTS=localhost,127.0.0.1
DJANGO_ENV=development

# PostgreSQL database
DB_HOST=localhost
DB_NAME=cap-recruiter_development
DB_USER=cap-recruiter
DB_PASSWORD=cap-recruiter

# Email server
EMAIL_HOST=localhost
EMAIL_PORT=1025

# PsyNet
EXPERIMENT_BASE_URL=http://localhost:5000
RECRUITER=DevCapRecruiter

# Websockets
WS_PROTOCOL=ws
WS_HOST=localhost:8000
```

### Setup PostgreSQL
```bash
sudo -u postgres -i
```
```bash
createuser -P cap-recruiter --createdb
```

Password: cap-recruiter

```bash
createdb -O cap-recruiter cap-recruiter_development
exit
```
```bash
sudo service postgresql reload
```

### Setup Django database
NOTICE: Make sure you are in the project's virtual environment before running the commands below

##### Create `admin` superuser:
```bash
python manage.py migrate
python manage.py createsuperuser --username admin --email admin@localhost
```

##### Create `api-user` user and authentication token
```bash
python manage.py createsuperuser --username api-admin --email api-admin@localhost
python manage.py drf_create_token api-admin
```
NOTICE: Save the generated token for later use!

E.g.: Generated token f9f9d8297c616440726a6c5b15f5aa750b80ed47 for user api-admin

You will need to export it later with
```bash
export CAP_RECRUITER_AUTH_TOKEN="Token <generated-token>"
```

### Startup processes
#### First terminal (daphne)
```bash
pipenv shell
python manage.py runserver
```

#### Second terminal (npm)
```bash
pipenv shell
cd frontend
npm run start
```

#### Third terminal (celery worker)
```bash
pipenv shell
celery -A experiments worker
```

#### Fourth terminal (celery beat)
```bash
pipenv shell
celery -A experiments beat
```


### Startup a PsyNet experiment
#### Fifth terminal
```bash
cd <YOUR_PSYNET_DIR>/demos/recruiters/cap_recruiter
workon <YOUR_PSYNET_VIRTUAL_ENV>
psynet debug --verbose
```

### PostreSQL
#### Log into database
```bash
sudo -u postgres psql cap-recruiter_development
```

## Add API token above to `~/.dallingerconfig`
