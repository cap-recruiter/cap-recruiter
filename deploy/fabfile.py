from fabric.api import cd, env, roles, run, task

# env variables
env.roledefs = {"web": ["jacoby.fg.ae.mpg.de"]}
env.user = "cap"
env.colorize_errors = True


# roles
@roles("web")
# tasks
@task
def deploy(branch="staging"):
    stage = "staging"
    if branch == "production":
        stage = "production"
    deploy_base_path = f"/home/cap/apps/cap-recruiter_{stage}"
    deploy_path = f"{deploy_base_path}/cap-recruiter"

    setup(deploy_base_path, deploy_path, stage)

    with cd(deploy_path):
        run("git fetch")
        run(f"git checkout {branch}")
        run("git pull")

        # install newest versions of pip and pipenv
        run("pip3 install --user --upgrade pip")
        run("pip3 install --user --upgrade pipenv")

        # run pipenv
        run("pipenv --version")
        # run('pipenv --rm') # only needed when upgrading Python
        run("pipenv run pipenv install --deploy")
        # run("pipenv run pipenv check")

        # collect static files
        run(
            f"pipenv run python manage.py collectstatic -v0 --noinput --settings=cap_recruiter.settings.{stage}"
        )

    # run migrations
    migrate(deploy_path, stage)

    # restart gunicorn
    run(f"supervisorctl restart cap-recruiter_{stage}_daphne")
    run(f"supervisorctl restart cap-recruiter_{stage}_celery")
    run(f"supervisorctl restart cap-recruiter_{stage}_celery_beat")


@task
def setup(deploy_base_path, deploy_path, stage):
    # run(f'mkdir -p {deploy_path}/cap_recruiter/static')
    run(f"mkdir -p {deploy_base_path}")
    with cd(deploy_base_path):
        run(
            "if ! [ -d cap-recruiter/.git ]; then git clone git@gitlab.com:computational-audition-lab/cap-recruiter.git; fi"
        )
    with cd(deploy_path):
        run(f"mkdir -p {deploy_path}/log")
        run(f"touch {deploy_path}/log/error.log")


def migrate(deploy_path, stage):
    with cd(deploy_path):
        run(
            f"pipenv run python manage.py migrate --database cap-recruiter_{stage} --settings=cap_recruiter.settings.{stage}"
        )
